package de.notepass.tape.jTapeAccess.model;

import de.notepass.tape.jTapeAccess.model.SimpleTapeDrive;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Extremely minimalistic OutputStream which uses the tape drive interface for communications
 */
public class TapeOutputStream extends OutputStream {
    private final SimpleTapeDrive tapeDevice;

    public TapeOutputStream(SimpleTapeDrive td) {
        tapeDevice = td;
    }

    @Override
    public void write(int i) throws IOException {
        tapeDevice.writeBytes(new byte[]{(byte)i});
    }

    @Override
    public void write(byte[] val, int off, int len) throws IOException {
        tapeDevice.writeBytes(val, off, len);
    }

    @Override
    public void write(byte[] val) throws IOException {
        tapeDevice.writeBytes(val);
    }

    @Override
    public void flush() throws IOException {
        tapeDevice.flushCaches();
    }
}
