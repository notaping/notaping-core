import de.notepass.notaping.core.fs.etfs.util.SerializeUtils;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class SerializeUtilTests {

    @Test
    public void testShortConversion() {
        short s = 12345;
        byte[] bytes = SerializeUtils.toBytes(s);
        short s2 = SerializeUtils.shortFromBytes(bytes);

        Assert.assertEquals("Original short and reconstructed short do not match", s, s2);
    }

    @Test
    public void testIntConversion() {
        int i = 1234567890;
        byte[] bytes = SerializeUtils.toBytes(i);
        long i2 = SerializeUtils.intFromBytes(bytes);

        Assert.assertEquals("Original int and reconstructed int do not match", i, i2);
    }

    @Test
    public void testLongConversion() {
        long l = 1234567890123456789L;
        byte[] bytes = SerializeUtils.toBytes(l);
        long l2 = SerializeUtils.longFromBytes(bytes);

        Assert.assertEquals("Original long and reconstructed long do not match", l, l2);
    }

}
