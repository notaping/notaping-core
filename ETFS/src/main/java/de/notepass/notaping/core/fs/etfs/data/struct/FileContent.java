package de.notepass.notaping.core.fs.etfs.data.struct;

import de.notepass.notaping.core.fs.etfs.data.EtfsInstance;
import de.notepass.notaping.core.fs.etfs.data.EtfsSerializableElement;
import de.notepass.notaping.core.fs.etfs.data.FileRuntimeMetaDataField;
import de.notepass.notaping.core.fs.etfs.data.listener.EtfsReadProgressCallback;
import de.notepass.notaping.core.fs.etfs.data.listener.EtfsWriteProgressCallback;
import de.notepass.notaping.core.fs.etfs.plugin.EtfsPlugin;
import de.notepass.notaping.core.fs.etfs.plugin.MetadataPosition;
import de.notepass.notaping.core.fs.etfs.util.SerializeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static de.notepass.notaping.core.fs.etfs.util.SerializeUtils.toBytes;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

/**
 * Java class representation of the "FileContent" struct, as defined in the ETFS specification.<br/>
 * Please note: As java still lacks proper unsigned support, the data types used here are "bigger" than
 * they should be according to the specification
 */
public class FileContent implements EtfsSerializableElement {
    // file_size: 8 int: Size of the file in bytes
    private long fileSize;

    // file_content: 0:9223372036854775807 byte: Content of the file. Size is dictated by the file_size filed in the TOC section
    // This will not be read to memory

    //runtime_meta: 4 int: Additional metadata resulting of possible runtime problems when reading/writing the file (See Metadata section for more info)
    private FileRuntimeMetaDataField runtimeMeta = new FileRuntimeMetaDataField();

    // plugin_data_length: 2 int: Number of entries in the plugin_data list
    private short pluginDataLength;

    // plugin_data: 0:32767 list<PluginCustomData>: Space for plugins to write custom data to (AFTER_FILE_CONTENT)
    private List<PluginCustomData> pluginData = new ArrayList<>();

    // Reference to the FileEntry struct this file belongs to
    private FileEntry fileEntryData;

    private static final Logger LOGGER = LoggerFactory.getLogger(FileContent.class);

    /**
     * file_size: 8 int: Size of the file in bytes
     * @return
     */
    public long getFileSize() {
        return fileSize;
    }

    /**
     * file_size: 8 int: Size of the file in bytes
     * @param fileSize
     */
    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }

    /**
     * runtime_meta: 4 int: Additional metadata resulting of possible runtime problems when reading/writing the file (See Metadata section for more info)
     * @return
     */
    public FileRuntimeMetaDataField getRuntimeMeta() {
        return runtimeMeta;
    }

    /**
     * runtime_meta: 4 int: Additional metadata resulting of possible runtime problems when reading/writing the file (See Metadata section for more info)
     * @param runtimeMeta
     */
    public void setRuntimeMeta(FileRuntimeMetaDataField runtimeMeta) {
        this.runtimeMeta = runtimeMeta;
    }

    /**
     * plugin_data_length: 2 int: Number of entries in the plugin_data list
     * @return
     */
    public short getPluginDataLength() {
        return pluginDataLength;
    }

    /**
     * plugin_data_length: 2 int: Number of entries in the plugin_data list
     * @param pluginDataLength
     */
    public void setPluginDataLength(short pluginDataLength) {
        this.pluginDataLength = pluginDataLength;
    }

    /**
     * plugin_data: 0:32767 list<PluginCustomData>: Space for plugins to write custom data to (AFTER_FILE_CONTENT)
     * @return
     */
    public List<PluginCustomData> getPluginData() {
        return pluginData;
    }

    /**
     * plugin_data: 0:32767 list<PluginCustomData>: Space for plugins to write custom data to (AFTER_FILE_CONTENT)
     * @param pluginData
     */
    public void setPluginData(List<PluginCustomData> pluginData) {
        this.pluginData = pluginData;
    }

    /**
     * Reference to the FileEntry struct this file belongs to
     * @return
     */
    public FileEntry getFileEntryData() {
        return fileEntryData;
    }

    /**
     * Reference to the FileEntry struct this file belongs to
     * @param fileEntryData
     */
    public void setFileEntryData(FileEntry fileEntryData) {
        this.fileEntryData = fileEntryData;
    }

    @Override
    public void serialize(OutputStream os, EtfsInstance context, EtfsWriteProgressCallback callback) throws IOException {
        // Step 1: Recalculate dynamic values


        // Step 2: Serialize
        os.write(toBytes(fileSize));
        callback.onStartWriteFile(fileEntryData.getTargetFile(), Files.size(fileEntryData.getTargetFile()));
        // Copies file. Try-Catch with resources makes sure that the InputStream is closed properly
        long numTransferredBytes = 0;
        // Write file content to tape
        try (InputStream is = Files.newInputStream(fileEntryData.getTargetFile())) {
            byte[] buffer = new byte[1024];

            int numBytesRead;
            while ((numBytesRead = is.read(buffer)) > 0) {
                if (numBytesRead == buffer.length) {
                    os.write(buffer);
                } else {
                    os.write(buffer, 0, numBytesRead);
                }

                numTransferredBytes += numBytesRead;
            }
        } catch (IOException e) {
            LOGGER.error("Error while writing file "+fileEntryData.getTargetFile().toString()+" to tape. Rest of file will be skipped and file will be marked with READ_ERROR", e);
            runtimeMeta.setFlag(FileRuntimeMetaDataField.RUNTIME_METADATA_FLAG.FILE_NOT_READABLE);
            callback.onUnreadableFile(fileEntryData.getTargetFile(), e);
        } finally {
            // Write missing byte, as else the reading side would overread
            SerializeUtils.writePaddingData(os, fileSize - numTransferredBytes);
        }

        os.write(toBytes(runtimeMeta.getRuntimeMetadata()));
        callback.onEndWriteFile(fileEntryData.getTargetFile(), fileSize);

        callback.onStartWritePluginCustomDataSection(MetadataPosition.AFTER_FILE_CONTENT, pluginDataLength);
        for (EtfsPlugin plugin : context.getPluginRegistry().getActivePlugins()) {
            if (plugin.wantsToWritePluginData(MetadataPosition.AFTER_FILE_CONTENT, this, getFileEntryData().getTargetFile())) {
                pluginData.add(new PluginCustomData(MetadataPosition.AFTER_FILE_CONTENT, this, getFileEntryData().getTargetFile()));
            }
        }
        pluginDataLength = (short) pluginData.size();
        os.write(toBytes(pluginDataLength));
        for (PluginCustomData pcd : pluginData) {
            pcd.serialize(os, context, callback);
        }
        callback.onEndWritePluginCustomDataSection(MetadataPosition.AFTER_FILE_CONTENT);
    }

    @Override
    public void deserialize(InputStream os, long version, EtfsInstance context, EtfsReadProgressCallback callback) throws IOException {
        //TODO
    }
}
