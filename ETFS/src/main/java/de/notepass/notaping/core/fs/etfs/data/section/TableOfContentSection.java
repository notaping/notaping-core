package de.notepass.notaping.core.fs.etfs.data.section;

import de.notepass.notaping.core.fs.etfs.data.EtfsInstance;
import de.notepass.notaping.core.fs.etfs.data.EtfsSerializableElement;
import de.notepass.notaping.core.fs.etfs.data.listener.EtfsReadProgressCallback;
import de.notepass.notaping.core.fs.etfs.data.listener.EtfsWriteProgressCallback;
import de.notepass.notaping.core.fs.etfs.data.struct.FileEntry;
import de.notepass.notaping.core.fs.etfs.util.SerializeUtils;

import javax.sql.rowset.serial.SerialStruct;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * Java class representation of the "Table of content" section, as defined in the ETFS specification.<br/>
 * Please note: As java still lacks proper unsigned support, the data types used here are "bigger" than
 * they should be according to the specification
 */
public class TableOfContentSection implements EtfsSerializableElement {
    // file_entries_length: 8 int: Number of entries in the file_entries list
    private long fileEntriesLength;

    // file_entries: 0:9223372036854775807 list<FileEntry>: List of file metadata present
    private List<FileEntry> fileEntries = new ArrayList<>();

    /**
     * file_entries_length: 8 int: Number of entries in the file_entries list
     * @return
     */
    public long getFileEntriesLength() {
        return fileEntriesLength;
    }

    /**
     * file_entries_length: 8 int: Number of entries in the file_entries list
     * @param fileEntriesLength
     */
    public void setFileEntriesLength(long fileEntriesLength) {
        this.fileEntriesLength = fileEntriesLength;
    }

    /**
     * file_entries: 0:9223372036854775807 list<FileEntry>: List of file metadata present
     * @return
     */
    public List<FileEntry> getFileEntries() {
        return fileEntries;
    }

    /**
     * file_entries: 0:9223372036854775807 list<FileEntry>: List of file metadata present
     * @param fileEntries
     */
    public void setFileEntries(List<FileEntry> fileEntries) {
        this.fileEntries = fileEntries;
    }

    @Override
    public void serialize(OutputStream os, EtfsInstance context, EtfsWriteProgressCallback callback) throws IOException {
        fileEntriesLength = getFileEntries().size();

        callback.onStartWriteTableOfContentSection(this);
        os.write(SerializeUtils.toBytes(fileEntriesLength));
        for (FileEntry fe : getFileEntries()) {
            fe.serialize(os, context, callback);
        }
        callback.onEndWriteTableOfContentSection(this);
    }

    @Override
    public void deserialize(InputStream os, long version, EtfsInstance context, EtfsReadProgressCallback callback) throws IOException {

    }
}
