package de.notepass.tape.jTapeAccess.model;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

// TODO: Implement skip() method
/**
 * Wrapper for the standard TapeOutputStream. Makes sure that all write operations happen in accordance with the block size<br/>
 * Can also buffer data access (Helpful when writing many smaller files)
 */
public class BufferedTapeOutputStream extends OutputStream {
    private byte[] buffer;
    private OutputStream delegate;
    private int pos;

    public BufferedTapeOutputStream(OutputStream delegate) {
        this(delegate, 64 * 1024);
    }

    public BufferedTapeOutputStream(OutputStream delegate, int numBytes) {
        buffer = new byte[numBytes];
        this.delegate = delegate;
    }

    public BufferedTapeOutputStream(OutputStream delegate, TapeInfo tapeInfo) {
        this(delegate, (int) tapeInfo.getBlockSizeBytes() * 1024);
    }

    @Override
    public void write(int b) throws IOException {
        ByteBuffer bb = ByteBuffer.allocate(Integer.BYTES);
        bb.putInt(b);
        write(bb.array());
    }

    @Override
    public void write(byte[] b) throws IOException {
        int srcPos = 0;
        int freeBufferSpace;

        while (!canFitData(b.length - srcPos)) {
            freeBufferSpace = getRemainingBufferSpace();
            byte[] subArr = new byte[freeBufferSpace];
            System.arraycopy(b, srcPos, buffer, pos, subArr.length);
            writeBufferToDelegate();
            srcPos += freeBufferSpace;
        }

        if (srcPos != b.length) {
            // Write remaining bytes if any
            int remainingBytesInInput = b.length - srcPos;
            System.arraycopy(b, srcPos, buffer, pos, remainingBytesInInput);
            pos += remainingBytesInInput;
        }
    }

    @Override
    public void write(byte[] b, int off, int len) throws IOException {
        byte[] subarr = new byte[len];
        System.arraycopy(b, off, subarr, 0, len);
        write(subarr);
    }

    @Override
    public void flush() throws IOException {
        //Fill remaining bytes with 255, as buffer size needs to be retained when writing to tape
        for (int i = pos; i < buffer.length; i++) {
            buffer[i] = Byte.MAX_VALUE;
        }

        writeBufferToDelegate();
    }

    private boolean canFitData(int size) {
        return getRemainingBufferSpace() >= size;
    }

    private int getRemainingBufferSpace() {
        return buffer.length - pos;
    }

    private void writeBufferToDelegate() throws IOException {
        delegate.write(buffer);
        Arrays.fill(buffer, (byte) 0);
        pos = 0;
    }
}
