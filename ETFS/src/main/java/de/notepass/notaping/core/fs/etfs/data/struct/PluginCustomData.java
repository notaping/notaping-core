package de.notepass.notaping.core.fs.etfs.data.struct;

import de.notepass.notaping.core.fs.etfs.data.EtfsInstance;
import de.notepass.notaping.core.fs.etfs.data.EtfsSerializableElement;
import de.notepass.notaping.core.fs.etfs.data.PluginMetaDataField;
import de.notepass.notaping.core.fs.etfs.data.listener.EtfsReadProgressCallback;
import de.notepass.notaping.core.fs.etfs.data.listener.EtfsWriteProgressCallback;
import de.notepass.notaping.core.fs.etfs.data.section.HeaderSection;
import de.notepass.notaping.core.fs.etfs.plugin.EtfsPlugin;
import de.notepass.notaping.core.fs.etfs.plugin.MetadataPosition;
import de.notepass.notaping.core.fs.etfs.util.SerializeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static de.notepass.notaping.core.fs.etfs.util.SerializeUtils.toBytes;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Java class representation of the "PluginCustomData" struct, as defined in the ETFS specification.<br/>
 * Please note: As java still lacks proper unsigned support, the data types used here are "bigger" than
 * they should be according to the specification
 */
public class PluginCustomData implements EtfsSerializableElement {
    // size: 4 int: Size of the entry, excluding this field
    private int size;

    // internal_id: 2 int: Reference to the internal ID from the header section
    private short internalId;

    // The data itself is not stored here, as it may be multiple GB per plugin. The plugin will later
    // get a reference to the tape stream from which it can read data and work with it in an optimised
    // way
    // content: 2147483000 byte: Custom data content. Advised to be string, but can be any data format

    // plugin_data_metadata: 4 int: Flags to manage problems that happened on write time (See plugin_data_metadata for more information)
    private PluginMetaDataField pluginDataMetadata = new PluginMetaDataField();

    // ========================== THE FOLLOWING DATA IS NOT SERIALIZED TO THE TAPE AND ONLY NEEDED FOR PROCESSING ==========================

    // Position of the metadata entry on the tape
    private MetadataPosition position;

    // Context element passed to the plugin when writing
    private Object[] context = new Object[0];

    private static final Logger LOGGER = LoggerFactory.getLogger(PluginCustomData.class);

    /**
     * Constructor with base information on the PCD position/context
     * @param position Position on the tape
     * @param context Element which is passed to the plugin as "context" element
     */
    public PluginCustomData(MetadataPosition position, Object ... context) {
        this.position = position;
        this.context = context;
    }

    /**
     * Returns the "size" field, which contains the number of bytes in the content field
     * @return
     */
    public int getSize() {
        return size;
    }

    /**
     * Sets the "size" field, which contains the number of bytes in the content field
     * @param size
     */
    public void setSize(int size) {
        this.size = size;
    }

    /**
     * Returns the remapped ID for the plugin which the data belongs to. The real IDs will be
     * remapped to short-values to save space. The real plugin-ID can be found in the Header part
     * TODO: Add reference to method call to resolve internal IDs to real IDs
     * @return
     */
    public short getInternalId() {
        return internalId;
    }

    /**
     * Sets the remapped ID for the plugin which the data belongs to. The real IDs will be
     * remapped to short-values to save space. The real plugin-ID can be found in the Header part
     * TODO: Add reference to method call to resolve internal IDs to real IDs
     * @return
     */
    public void setInternalId(short internalId) {
        this.internalId = internalId;
    }

    /**
     * Returns the position of the PCD element on the tape
     * @return
     */
    public MetadataPosition getPosition() {
        return position;
    }

    /**
     * Returns the position of the PCD element on the tape
     * @return
     */
    public void setPosition(MetadataPosition position) {
        this.position = position;
    }

    /**
     * Returns the element which is passed to the plugin as "context" element
     * @return
     */
    public Object[] getContext() {
        return context;
    }

    /**
     * Sets the element which is passed to the plugin as "context" element
     * @param context
     */
    public void setContext(Object ... context) {
        this.context = context;
    }

    /**
     * plugin_data_metadata: 4 int: Flags to manage problems that happened on write time (See plugin_data_metadata for more information)
     * @return
     */
    public PluginMetaDataField getPluginDataMetadata() {
        return pluginDataMetadata;
    }

    /**
     * plugin_data_metadata: 4 int: Flags to manage problems that happened on write time (See plugin_data_metadata for more information)
     * @param pluginDataMetadata
     */
    public void setPluginDataMetadata(PluginMetaDataField pluginDataMetadata) {
        this.pluginDataMetadata = pluginDataMetadata;
    }

    /**
     * Casts the classes for using the correct "wantsToWritePluginData" method and extracts the metadata
     * @return
     */
    private boolean dispatchWriteCheck(EtfsInstance etfs) {
        switch (position) {
            case AFTER_FILE_CONTENT:
                if (context.length <= 0) {
                    LOGGER.warn("Missing 2 of 2 context elements to pass to plugin (FileContent and File). Substituting null. Will probably lead to NPEs");
                    return etfs.getPluginRegistry().get(internalId).wantsToWritePluginData(position, null, null);
                } else if (context.length == 1) {
                    LOGGER.warn("Missing 1 of 2 context elements to pass to plugin (File). Substituting null. Will probably lead to NPEs");
                    return etfs.getPluginRegistry().get(internalId).wantsToWritePluginData(position, (FileContent) context[0], null);
                } else {
                    return etfs.getPluginRegistry().get(internalId).wantsToWritePluginData(position, (FileContent) context[0], (Path) context[1]);
                }
            case AFTER_HEADER:
                if (context.length <= 0) {
                    LOGGER.warn("Missing 1 of 1 context elements to pass to plugin (HeaderSection). Substituting null. Will probably lead to NPEs");
                    return etfs.getPluginRegistry().get(internalId).wantsToWritePluginData(position, (HeaderSection) null);
                } else if (context.length == 1) {
                    return etfs.getPluginRegistry().get(internalId).wantsToWritePluginData(position, (HeaderSection) context[0]);
                }
            case AFTER_TOC_ENTRY:
                if (context.length <= 0) {
                    LOGGER.warn("Missing 1 of 1 context elements to pass to plugin (FileEntry). Substituting null. Will probably lead to NPEs");
                    return etfs.getPluginRegistry().get(internalId).wantsToWritePluginData(position, (FileEntry) null);
                } else if (context.length == 1) {
                    return etfs.getPluginRegistry().get(internalId).wantsToWritePluginData(position, (FileEntry) context[0]);
                }
            default:
                LOGGER.warn("Unknown PluginCustomData-Position for mapping dispatchWriteCheck: " + position);
                return false;
        }
    }

    /**
     * Casts the classes for using the correct "writePluginData" method and extracts the metadata
     * @return
     */
    private void dispatchWriteCall(EtfsInstance etfs, OutputStream os) throws IOException {
        switch (position) {
            case AFTER_FILE_CONTENT:
                if (context.length <= 0) {
                    LOGGER.warn("Missing 2 of 2 context elements to pass to plugin (FileContent and File). Substituting null. Will probably lead to NPEs");
                    etfs.getPluginRegistry().get(internalId).writePluginData(position, null, null, os);
                } else if (context.length == 1) {
                    LOGGER.warn("Missing 1 of 2 context elements to pass to plugin (File). Substituting null. Will probably lead to NPEs");
                    etfs.getPluginRegistry().get(internalId).writePluginData(position, (FileContent) context[0], null, os);
                } else {
                    etfs.getPluginRegistry().get(internalId).writePluginData(position, (FileContent) context[0], (Path) context[1], os);
                }
                break;
            case AFTER_HEADER:
                if (context.length <= 0) {
                    LOGGER.warn("Missing 1 of 1 context elements to pass to plugin (HeaderSection). Substituting null. Will probably lead to NPEs");
                    etfs.getPluginRegistry().get(internalId).writePluginData(position, (HeaderSection) null, os);
                } else {
                    etfs.getPluginRegistry().get(internalId).writePluginData(position, (HeaderSection) context[0], os);
                }
                break;
            case AFTER_TOC_ENTRY:
                if (context.length <= 0) {
                    LOGGER.warn("Missing 1 of 1 context elements to pass to plugin (FileEntry). Substituting null. Will probably lead to NPEs");
                    etfs.getPluginRegistry().get(internalId).writePluginData(position, (FileEntry) null, os);
                } else {
                    etfs.getPluginRegistry().get(internalId).writePluginData(position, (FileEntry) context[0], os);
                }
                break;
            default:
                LOGGER.warn("Unknown PluginCustomData-Position for mapping dispatchWriteCall: " + position);
        }
    }

    @Override
    public void serialize(OutputStream os, EtfsInstance context, EtfsWriteProgressCallback callback) throws IOException {
        EtfsPlugin plugin = context.getPluginRegistry().get(internalId);

        callback.onStartWritePluginCustomData(position, plugin);

        // Step 1: Write plugin data to temporary file, as the size needs to be known before writing to tape
        Path pcdTempFile = SerializeUtils.getTempFile();
        boolean pluginError = false;
        try (OutputStream fos = Files.newOutputStream(pcdTempFile)) {
            dispatchWriteCall(context, fos);
        } catch (Exception e) {
            Files.delete(pcdTempFile);

            if (e instanceof IOException) {
                throw e;
            } else {
                // TODO: Error flag
                LOGGER.error("Unhandled exception in plugin \""+plugin.getID()+"\", no custom data will be written", e);
                callback.onUnhandledPluginException(plugin, e);
                pluginError = true;
                pluginDataMetadata.setFlag(PluginMetaDataField.PLUGIN_METADATA_FLAG.PLUGIN_ERROR);
            }
        }

        // Step 2: Prepare values
        if (!pluginError) {
            size = (int) Files.size(pcdTempFile);
        } else {
            size = 0;
        }

        // Step 3: Write values
        os.write(toBytes(size));
        os.write(toBytes(internalId));
        if (!pluginError) {
            try (InputStream is = Files.newInputStream(pcdTempFile)) {
                is.transferTo(os);
            } finally {
                Files.delete(pcdTempFile);
            }
        }

        os.write(toBytes(pluginDataMetadata.getPluginDataMetadata()));

        callback.onEndWritePluginCustomData(position, plugin);
    }

    @Override
    public void deserialize(InputStream os, long version, EtfsInstance context, EtfsReadProgressCallback callback) throws IOException {

    }
}
