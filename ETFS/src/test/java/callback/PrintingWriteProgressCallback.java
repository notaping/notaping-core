package callback;

import de.notepass.notaping.core.fs.etfs.data.listener.EtfsWriteProgressCallback;
import de.notepass.notaping.core.fs.etfs.data.section.HeaderSection;
import de.notepass.notaping.core.fs.etfs.data.section.PluginConfigSection;
import de.notepass.notaping.core.fs.etfs.data.section.TableOfContentSection;
import de.notepass.notaping.core.fs.etfs.data.struct.FileEntry;
import de.notepass.notaping.core.fs.etfs.plugin.EtfsPlugin;
import de.notepass.notaping.core.fs.etfs.plugin.MetadataPosition;

import java.io.IOException;
import java.nio.file.Path;

public class PrintingWriteProgressCallback implements EtfsWriteProgressCallback {
    @Override
    public void onStartWriteHeaderSection(HeaderSection headerSection) {
        System.out.println("onStartWriteHeaderSection()");
    }

    @Override
    public void onEndWriteHeaderSection(HeaderSection headerSection) {
        System.out.println("onEndWriteHeaderSection()");
    }

    @Override
    public void onStartWritePluginConfigSection(PluginConfigSection pluginConfigSection) {
        System.out.println("onStartWritePluginConfigSection()");
    }

    @Override
    public void onEndWritePluginConfigSection(PluginConfigSection pluginConfigSection) {
        System.out.println("onEndWritePluginConfigSection()");
    }

    @Override
    public void onStartWritePluginInfo(EtfsPlugin plugin) {
        System.out.println("onStartWritePluginInfo(id="+plugin.getID()+")");
    }

    @Override
    public void onEndWritePluginInfo(EtfsPlugin plugin) {
        System.out.println("onEndWritePluginInfo(id="+plugin.getID()+")");
    }

    @Override
    public void onStartWritePluginConfiguration(EtfsPlugin plugin) {
        System.out.println("onStartWritePluginConfiguration(id="+plugin.getID()+")");
    }

    @Override
    public void onEndWritePluginConfiguration(EtfsPlugin plugin) {
        System.out.println("onEndWritePluginConfiguration(id="+plugin.getID()+")");
    }

    @Override
    public void onStartWriteTableOfContentSection(TableOfContentSection tableOfContentSection) {
        System.out.println("onStartWriteTableOfContentSection()");
    }

    @Override
    public void onStartWriteTableOfContentsEntry(FileEntry entry) {
        System.out.println("onStartWriteTableOfContentsEntry()");
    }

    @Override
    public void onEndWriteTableOfContentsEntry(FileEntry entry) {
        System.out.println("onEndWriteTableOfContentsEntry()");
    }

    @Override
    public void onEndWriteTableOfContentSection(TableOfContentSection tableOfContentSection) {
        System.out.println("onEndWriteTableOfContentSection()");
    }

    @Override
    public void onStartWriteFileSection(long entries) {
        System.out.println("onStartWriteFileSection()");
    }

    @Override
    public void onEndWriteFileSection() {
        System.out.println("onEndWriteFileSection()");
    }

    @Override
    public void onStartWriteFile(Path target, long fileSize) {
        System.out.println("onStartWriteFile(path="+target+")");
    }

    @Override
    public void onEndWriteFile(Path target, long fileSize) {
        System.out.println("onEndWriteFile(path="+target+")");
    }

    @Override
    public void onUnreadableFile(Path target, IOException e) {
        System.out.println("onEndWriteFile(path="+target+")");
        e.printStackTrace();
    }

    @Override
    public void onFileProgress(Path target, long currentPosition, long fileSize) {
        System.out.println("onFileProgress(path="+target+", currentPosition="+currentPosition+", fileSize="+fileSize+")");
    }

    @Override
    public void onStartWritePluginCustomDataSection(MetadataPosition metadataPosition, short totalEntries) {
        System.out.println("onStartWritePluginCustomDataSection(pos="+metadataPosition+")");
    }

    @Override
    public void onEndWritePluginCustomDataSection(MetadataPosition metadataPosition) {
        System.out.println("onEndWritePluginCustomDataSection(pos="+metadataPosition+")");
    }

    @Override
    public void onStartWritePluginCustomData(MetadataPosition metadataPosition, EtfsPlugin plugin) {
        System.out.println("onStartWritePluginCustomData(pos="+metadataPosition+", id="+plugin.getID()+")");
    }

    @Override
    public void onEndWritePluginCustomData(MetadataPosition metadataPosition, EtfsPlugin plugin) {
        System.out.println("onEndWritePluginCustomData(pos="+metadataPosition+", id="+plugin.getID()+")");
    }

    @Override
    public void onWriteAbort() {
        System.out.println("onWriteAbort()");
    }

    @Override
    public void onUnhandledPluginException(EtfsPlugin plugin, Exception e) {
        System.out.println("onStartWritePluginCustomData(id="+plugin.getID()+")");
        e.printStackTrace();
    }
}
