package de.notepass.notaping.core.fs.etfs.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.SortedMap;

public class SerializeUtils {
    /**
     * Returns size in bytes of an integer
     * @param i
     * @return
     */
    public static int sizeof(int i) {
        return Integer.BYTES;
    }

    /**
     * Returns size in bytes of a long
     * @param i
     * @return
     */
    public static int sizeof(long i) {
        return Long.BYTES;
    }

    /**
     * Returns size in bytes of a short
     * @param i
     * @return
     */
    public static int sizeof(short i) {
        return Short.BYTES;
    }

    /**
     * Returns size in bytes of a byte
     * @param i
     * @return
     */
    public static int sizeof(byte i) {
        return Byte.BYTES;
    }

    /**
     * Returns the size of the UTF-8 encoded version of the given string
     * @param i String to calculate the size of
     * @return
     */
    public static int sizeof(String i) {
        return i.getBytes(StandardCharsets.UTF_8).length;
    }

    /**
     * Reads a short from the input stream and returns it
     * @param is InputStream to read from
     * @return next integer in the stream
     * @throws IOException On stream error
     */
    public static short readNextShort(InputStream is) throws IOException {
        byte[] buffer = new byte[2];
        is.read(buffer);
        return shortFromBytes(buffer);
    }

    /**
     * Assembled a byte array into a short
     * @param bytes Bytes to assemble
     * @return Assembled short
     */
    public static short shortFromBytes(byte[] bytes) {
        return shortFromBytes(bytes, 0);
    }

    /**
     * Assembled a byte array into a short
     * @param bytes Bytes to assemble
     * @return Assembled short
     */
    public static short shortFromBytes(byte[] bytes, int offset) {
        return (short) (((short) bytes[1 + offset] << 8)
                | ((short) bytes[offset] & 0xff));
    }

    /**
     * Reads a byte from the input stream and returns it
     * @param is InputStream to read from
     * @return next byte in the stream
     * @throws IOException On stream error
     */
    public static byte readNextByte(InputStream is) throws IOException {
        byte[] buffer = new byte[1];
        is.read(buffer);
        return buffer[0];
    }

    /**
     * Reads an integer from the input stream and returns it
     * @param is InputStream to read from
     * @return next integer in the stream
     * @throws IOException On stream error
     */
    public static int readNextInt(InputStream is) throws IOException {
        byte[] buffer = new byte[4];
        is.read(buffer);
        return intFromBytes(buffer);
    }

    /**
     * Assembled a byte array into an int
     * @param bytes Bytes to assemble
     * @return Assembled int
     */
    public static int intFromBytes(byte[] bytes) {
        return intFromBytes(bytes, 0);
    }

    /**
     * Assembled a byte array into an int
     * @param bytes Bytes to assemble
     * @return Assembled int
     */
    public static int intFromBytes(byte[] bytes, int offset) {
        return ((int) bytes[3 + offset] << 24)
                | ((int) bytes[2 + offset] & 0xff) << 16
                | ((int) bytes[1 + offset] & 0xff) << 8
                | ((int) bytes[offset] & 0xff);

    }

    /**
     * Reads a long from the input stream and returns it
     * @param is InputStream to read from
     * @return next long in the stream
     * @throws IOException On stream error
     */
    public static long readNextLong(InputStream is) throws IOException {
        byte[] buffer = new byte[8];
        is.read(buffer);
        return longFromBytes(buffer);
    }

    /**
     * Assembled a byte array into a long
     * @param bytes Bytes to assemble
     * @return Assembled long
     */
    public static long longFromBytes(byte[] bytes) {
        return longFromBytes(bytes, 0);
    }

    /**
     * Assembled a byte array into a long
     * @param bytes Bytes to assemble
     * @return Assembled long
     */
    public static long longFromBytes(byte[] bytes, int offset) {
        return ((long) bytes[7 + offset] << 56)
                | ((long) bytes[6 + offset] & 0xff) << 48
                | ((long) bytes[5 + offset] & 0xff) << 40
                | ((long) bytes[4 + offset] & 0xff) << 32
                | ((long) bytes[3 + offset] & 0xff) << 24
                | ((long) bytes[2 + offset] & 0xff) << 16
                | ((long) bytes[1 + offset] & 0xff) << 8
                | ((long) bytes[offset] & 0xff);

    }

    /**
     * Reads a string from the input stream and returns it
     * @param is InputStream to read from
     * @param length Size of the string to read
     * @return next string in the stream
     * @throws IOException On stream error
     */
    public static String readNextString(InputStream is, int length) throws IOException {
        byte[] buffer = new byte[length];
        is.read(buffer);
        return stringFromBytes(buffer);
    }

    /**
     * Assembled a byte array into a string
     * @param bytes Bytes to assemble
     * @return Assembled strin
     */
    public static String stringFromBytes(byte[] bytes) {
        return new String(bytes, StandardCharsets.UTF_8);
    }

    /**
     * Assembled a byte array into a string
     * @param bytes Bytes to assemble
     * @return Assembled strin
     */
    public static String stringFromBytes(byte[] bytes, int offset, int length) {
        return new String(bytes, offset, length, StandardCharsets.UTF_8);
    }

    /**
     * Disassembled a short into a byte array
     * @param s Short to disassemble
     * @return Disassembled short
     */
    public static byte[] toBytes(short s) {
        return new byte[]{
                (byte) s,
                (byte) (s >> 8)
        };
    }

    /**
     * Disassembled an int into a byte array
     * @param i Int to disassemble
     * @return Disassembled int
     */
    public static byte[] toBytes(int i) {
        return new byte[]{
                (byte) i,
                (byte) (i >> 8),
                (byte) (i >> 16),
                (byte) (i >> 24)
        };
    }

    /**
     * Disassembled a long into a byte array
     * @param l Long to disassemble
     * @return Disassembled short
     */
    public static byte[] toBytes(long l) {
        return new byte[]{
                (byte) l,
                (byte) (l >> 8),
                (byte) (l >> 16),
                (byte) (l >> 24),
                (byte) (l >> 32),
                (byte) (l >> 40),
                (byte) (l >> 48),
                (byte) (l >> 56)
        };
    }

    /**
     * Disassembled a byte into a byte array
     * @param b Byte to disassemble
     * @return Disassembled byte
     */
    public static byte[] toBytes(byte b) {
        ByteBuffer bb = ByteBuffer.allocate(Byte.BYTES);
        bb.put(b);
        return bb.array();
    }

    /**
     * Disassembled a boolean into a byte array
     * @param bool Boolean to disassemble
     * @return Disassembled boolean
     */
    public static byte[] toBytes(boolean bool) {
        byte b = 0;
        if (bool) {
            b = 1;
        }

        return toBytes(b);
    }

    /**
     * Disassembled a string into an UTF-8 encoded byte array
     * @param s String to disassemble
     * @return Disassembled string
     */
    public static byte[] toBytes(String s) {
        return s.getBytes(StandardCharsets.UTF_8);
    }

    public static Path getTempFile() throws IOException {
        return Files.createTempFile("ETFS-TEMPDATA-", ".tmp");
    }

    /**
     * Normalizes the file path for saving on tape
     * @param p
     * @return
     */
    public static String normalizeFilePath(Path p) {
        //TODO
        // WIN:/C/a/b/c
        // UNIX: /a/b/c
        return p.toString();
    }

    /**
     * Writes the given amount of 0 bytes to the output stream
     * @param os
     * @param numBytes
     * @throws IOException
     */
    public static void writePaddingData(OutputStream os, long numBytes) throws IOException {
        long numRemainingBytes = numBytes;
        byte[] buffer = new byte[1024];
        while (numRemainingBytes > 0) {
            if (buffer.length <= numRemainingBytes) {
                // Write 0 buffer. Compression will be happy :)
                os.write(buffer);
                numRemainingBytes -= buffer.length;
            } else {
                os.write(buffer, 0, (int) numRemainingBytes);
                numRemainingBytes = 0;
            }
        }
    }
}
