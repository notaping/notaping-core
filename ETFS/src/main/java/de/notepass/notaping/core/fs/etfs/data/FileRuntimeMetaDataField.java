package de.notepass.notaping.core.fs.etfs.data;

public class FileRuntimeMetaDataField {
    public enum RUNTIME_METADATA_FLAG {
        /**
         * Error state indicating that the file couldn't be read from the source filesystem
         */
        FILE_NOT_READABLE(0x000001),

        /**
         * Error state indicating that there was an error while writing the file to tape
         */
        FILE_NOT_WRITABLE(0x000002);

        private int mask;

        RUNTIME_METADATA_FLAG(int mask) {
            this.mask = mask;
        }

        public int getSetMask() {
            return mask;
        }

        public int getUnsetMask() {
            return mask ^ 0xFFFFFF;
        }
    }

    private int runtimeMetadata;

    public void setFlag(RUNTIME_METADATA_FLAG flag) {
        runtimeMetadata |= flag.getSetMask();
    }

    public boolean getFlag(RUNTIME_METADATA_FLAG flag) {
        return (runtimeMetadata & flag.getSetMask()) != 0;
    }

    public void unsetFlag(RUNTIME_METADATA_FLAG flag) {
        runtimeMetadata &= flag.getUnsetMask();
    }

    public int getRuntimeMetadata() {
        return runtimeMetadata;
    }

    public void setRuntimeMetadata(int runtimeMetadata) {
        this.runtimeMetadata = runtimeMetadata;
    }
}
