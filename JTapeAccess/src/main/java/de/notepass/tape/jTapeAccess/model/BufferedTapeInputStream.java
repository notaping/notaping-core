package de.notepass.tape.jTapeAccess.model;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/**
 * Wrapper for the standard TapeInputStream. Makes sure that all read operations happen in accordance with the block size<br/>
 * Can also buffer data access (Helpful when reading many smaller files)
 */
public class BufferedTapeInputStream extends InputStream {
    private InputStream source;
    private byte[] buffer;
    private int position = Integer.MAX_VALUE;

    public BufferedTapeInputStream(InputStream dataSource, int bufferSizeBytes) {
        this.source = dataSource;
        buffer = new byte[bufferSizeBytes];
    }

    public BufferedTapeInputStream(InputStream dataSource) {
        this(dataSource, 10 * 1024 * 1024);
    }

    @Override
    public int read() throws IOException {
        byte[] i = new byte[4];
        read(i);
        return ByteBuffer.allocate(Integer.BYTES).put(i).getInt();
    }

    @Override
    public long skip(long n) throws IOException {
        long numRemainingBytes =  n;

        if (bytesAvailable() <= 0) {
            readNextBufferContent();
        }

        while (!dataIsAvailable(numRemainingBytes)) {
            int availableBytes = bytesAvailable();
            //Buffer empty, read in new data
            readNextBufferContent();

            numRemainingBytes -= availableBytes;
        }

        if (numRemainingBytes > 0) {
            position = (int) numRemainingBytes;
        }

        return n;
    }

    @Override
    public int read(byte[] b) throws IOException {
        int numRemainingBytes =  b.length;

        if (bytesAvailable() <= 0) {
            readNextBufferContent();
        }

        while (!dataIsAvailable(numRemainingBytes)) {
            int availableBytes = bytesAvailable();
            System.arraycopy(buffer, position, b, (b.length - numRemainingBytes), availableBytes);
            //Buffer empty, read in new data
            readNextBufferContent();

            numRemainingBytes -= availableBytes;
        }

        if (numRemainingBytes > 0) {
            System.arraycopy(buffer, position, b, (b.length - numRemainingBytes), numRemainingBytes);
            position += numRemainingBytes;
            numRemainingBytes = 0;
        }

        return b.length - numRemainingBytes;
    }

    @Override
    public int read(byte[] b, int off, int len) throws IOException {
        byte[] subarr = new byte[len];

        int numRead = read(subarr);

        System.arraycopy(subarr, 0, b, off, numRead);

        return numRead;
    }

    private int bytesAvailable() {
        return buffer.length - position;
    }

    private boolean dataIsAvailable(long numBytes) {
        return bytesAvailable() >= numBytes;
    }

    private void readNextBufferContent() throws IOException {
        int totalBytesRead = 0;
        while (totalBytesRead < buffer.length) {
            totalBytesRead += source.read(buffer, totalBytesRead, (buffer.length - totalBytesRead));
        }

        position = 0;
    }
}
