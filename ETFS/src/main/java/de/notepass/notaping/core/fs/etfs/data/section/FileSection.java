package de.notepass.notaping.core.fs.etfs.data.section;

import de.notepass.notaping.core.fs.etfs.data.EtfsInstance;
import de.notepass.notaping.core.fs.etfs.data.EtfsSerializableElement;
import de.notepass.notaping.core.fs.etfs.data.listener.EtfsReadProgressCallback;
import de.notepass.notaping.core.fs.etfs.data.listener.EtfsWriteProgressCallback;
import de.notepass.notaping.core.fs.etfs.data.struct.FileContent;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public class FileSection implements EtfsSerializableElement {
    // files: 0:9223372036854775807 List<FileContent>: List of files
    private List<FileContent> files = new ArrayList<>();

    /**
     * files: 0:9223372036854775807 List<FileContent>: List of files
     * @return
     */
    public List<FileContent> getFiles() {
        return files;
    }

    /**
     * files: 0:9223372036854775807 List<FileContent>: List of files
     * @param files
     */
    public void setFiles(List<FileContent> files) {
        this.files = files;
    }

    @Override
    public void serialize(OutputStream os, EtfsInstance context, EtfsWriteProgressCallback callback) throws IOException {
        callback.onStartWriteFileSection(files.size());
        for (FileContent fc:files) {
            fc.serialize(os, context, callback);
        }
        callback.onEndWriteFileSection();
    }

    @Override
    public void deserialize(InputStream os, long version, EtfsInstance context, EtfsReadProgressCallback callback) {
        //TODO
    }
}
