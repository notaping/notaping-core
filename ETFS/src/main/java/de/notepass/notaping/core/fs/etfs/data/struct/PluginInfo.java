package de.notepass.notaping.core.fs.etfs.data.struct;

import de.notepass.notaping.core.fs.etfs.data.EtfsInstance;
import de.notepass.notaping.core.fs.etfs.data.EtfsSerializableElement;
import de.notepass.notaping.core.fs.etfs.data.listener.EtfsReadProgressCallback;
import de.notepass.notaping.core.fs.etfs.data.listener.EtfsWriteProgressCallback;
import de.notepass.notaping.core.fs.etfs.util.SerializeUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Java class representation of the "PluginInfo" struct, as defined in the ETFS specification.<br/>
 * Please note: As java still lacks proper unsigned support, the data types used here are "bigger" than
 * they should be according to the specification
 */
public class PluginInfo implements EtfsSerializableElement {
    // size: 4 int: Size of the entry, excluding this field
    private int size;

    // version: 4 int: Version of the plugin used
    private int version;

    // id_size: 2 int: Size of the identifier string of the plugin (See the "creating a plugin" doc for more information on this)
    private short idSize;

    // id: 1:32767 string: ID of the plugin which is described in this section
    private String id;

    // internal_id: 2 int: Remapped ID to a 16-bit integer. This is done to save space, as this ID will be referenced instead of the
    // (probably) longer "real" ID. This also means, that a max of 32767 plugins can be active per tape.
    private short internalId;

    /**
     * size: 4 int: Size of the entry, excluding this field
     * @return
     */
    public int getSize() {
        return size;
    }

    /**
     * size: 4 int: Size of the entry, excluding this field
     * @param size
     */
    public void setSize(int size) {
        this.size = size;
    }

    /**
     * version: 4 int: Version of the plugin used to create this entry
     * @return
     */
    public int getVersion() {
        return version;
    }

    /**
     * version: 4 int: Version of the plugin used to create this entry
     * @param version
     */
    public void setVersion(int version) {
        this.version = version;
    }

    /**
     * id_size: 2 int: Size of the identifier string of the plugin (See the "creating a plugin" doc for more information on this)
     * @return
     */
    public short getIdSize() {
        return idSize;
    }

    /**
     * id_size: 2 int: Size of the identifier string of the plugin (See the "creating a plugin" doc for more information on this)
     * @param idSize
     */
    public void setIdSize(short idSize) {
        this.idSize = idSize;
    }

    /**
     * id: 1:32767 string: ID of the plugin which is described in this section (See the "creating a plugin" doc for more information on this)
     * @return
     */
    public String getId() {
        return id;
    }

    /**
     * id: 1:32767 string: ID of the plugin which is described in this section (See the "creating a plugin" doc for more information on this)
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * internal_id: 2 int: Remapped ID to a 16-bit integer. This is done to save space, as this ID will be referenced instead of the
     *    (probably) longer "real" ID. This also means, that a max of 32767 plugins can be active per tape. (See the "creating a plugin" doc for more information on this)
     * @return
     */
    public short getInternalId() {
        return internalId;
    }

    /**
     * internal_id: 2 int: Remapped ID to a 16-bit integer. This is done to save space, as this ID will be referenced instead of the
     * (probably) longer "real" ID. This also means, that a max of 32767 plugins can be active per tape. (See the "creating a plugin" doc for more information on this)
     * @param internalId
     */
    public void setInternalId(short internalId) {
        this.internalId = internalId;
    }

    @Override
    public void serialize(OutputStream os, EtfsInstance context, EtfsWriteProgressCallback callback) throws IOException {
        idSize = (short) SerializeUtils.sizeof(id);

        callback.onStartWritePluginInfo(context.getPluginRegistry().get(id));
        os.write(SerializeUtils.toBytes(size));
        os.write(SerializeUtils.toBytes(version));
        os.write(SerializeUtils.toBytes(idSize));
        os.write(SerializeUtils.toBytes(id));
        os.write(SerializeUtils.toBytes(internalId));
        callback.onEndWritePluginInfo(context.getPluginRegistry().get(id));
    }

    @Override
    public void deserialize(InputStream os, long version, EtfsInstance context, EtfsReadProgressCallback callback) throws IOException {

    }
}
