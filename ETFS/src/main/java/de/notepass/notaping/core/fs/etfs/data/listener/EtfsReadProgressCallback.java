package de.notepass.notaping.core.fs.etfs.data.listener;

import de.notepass.notaping.core.fs.etfs.data.section.HeaderSection;
import de.notepass.notaping.core.fs.etfs.data.section.PluginConfigSection;
import de.notepass.notaping.core.fs.etfs.data.section.TableOfContentSection;
import de.notepass.notaping.core.fs.etfs.data.struct.FileEntry;
import de.notepass.notaping.core.fs.etfs.plugin.EtfsPlugin;
import de.notepass.notaping.core.fs.etfs.plugin.MetadataPosition;

import java.nio.file.Path;

public interface EtfsReadProgressCallback {
    /**
     * The amount of bytes read after which {@link #onFileProgress(Path, long, long)} is triggered
     */
    public long reportByteSize = 64*1024;

    // ================== Header section ==================

    /**
     * This method is called before parsing the header section
     */
    public void onStartReadHeaderSection();

    /**
     * This method is called after the header section was read an parsed
     * @param headerSection HeaderSection which was read
     */
    public void onEndReadHeaderSection(HeaderSection headerSection);

    // ================== Plugin config section ==================

    /**
     * This method is called before the "Plugin config section" is read
     */
    public void onStartReadPluginConfigSection();

    /**
     * This method is called after the "Plugin config section" is read and parsed
     * @param pluginConfigSection Parsed pluginconfig section data
     */
    public void onEndReadPluginConfigSection(PluginConfigSection pluginConfigSection);

    /**
     * This method is called, if an existing plugin was found on the tape and parsed
     * @param plugin Parsed plugin data
     */
    public void onPluginDiscovered(EtfsPlugin plugin);

    /**
     * This method is called for every plugin that has a configuration set saved on the tape. The method is called,
     * after the plugin has parsed its data
     * @param plugin Plugin which was reconfigured
     */
    public void onPluginReconfigured(EtfsPlugin plugin);

    /**
     * This method is called, if an plugin was found on the tape which isn't available locally
     * @param pluginId ID of the plugin saved on the tape
     */
    public void onMissingPluginDiscovered(String pluginId);

    // ================== TOC section ==================

    /**
     * This method is called before the table of contents section is read and parsed
     * @param numberOfEntries The number of entries in the TOC section
     */
    public void onStartReadTableOfContentSection(long numberOfEntries);

    /**
     * This method is called before a file entry is read from the TOC
     */
    public void onStartReadTableOfContentsEntry();

    /**
     * This method is called after a file entry was read from the TOC
     * @param entry The parsed file entry
     */
    public void onEndReadTableOfContentsEntry(FileEntry entry);

    /**
     * This method is called after the table of contents section is parsed
     * @param tableOfContentSection The parsed section data
     */
    public void onEndReadTableOfContentSection(TableOfContentSection tableOfContentSection);

    // ================== File section ==================

    /**
     * This method is called before the file section is read
     * @param entries Number of files in the file section. Please note, that they don't have to be on the same tape,
     *                as multitape volumes exist
     */
    public void onStartReadFileSection(long entries);

    /**
     * This method is called after the file section is read
     */
    public void onEndReadFileSection();

    /**
     * This method is called before processing a file
     * @param target Reference to the file target
     * @param fileSize Size of the file
     */
    public void onStartReadFile(Path target, long fileSize);

    /**
     * This method is called after processing a file
     * @param target Reference to the file target
     * @param fileSize Size of the file
     */
    public void onEndReadFile(Path target, long fileSize);

    /**
     * This method is called if a file on the tape cannot be processed (e.g. because it couldn't be written to the tape)
     */
    public void onUnreadableFile();

    /**
     * This method is called while processing files. It is called after {@link #reportByteSize} bytes where read from the tape
     * @param target Reference to the file target
     * @param currentPosition Amount of bytes already read
     * @param fileSize Overall size of the file
     */
    public void onFileProgress(Path target, long currentPosition, long fileSize);

    // ================== Plugin custom data section ==================

    /**
     * This method is called before the plugin custom data block (=list) at a specific position is read
     * @param metadataPosition Which position the metadata is at
     * @param totalEntries The total amount of entries in the list
     */
    public void onStartReadPluginCustomDataSection(MetadataPosition metadataPosition, short totalEntries);

    /**
     * This method is called after the plugin custom data block (=list) at a specific position is read
     * @param metadataPosition Which position the metadata is at
     */
    public void onEndReadPluginCustomDataSection(MetadataPosition metadataPosition);

    /**
     * This method is called before a single plugin custom data entry is read
     * @param metadataPosition Which position the metadata is at
     * @param plugin The plugin which will read the data
     */
    public void onStartReadPluginCustomData(MetadataPosition metadataPosition, EtfsPlugin plugin);

    /**
     * This method is called after a single plugin custom data entry is read
     * @param metadataPosition Which position the metadata is at
     * @param plugin The plugin which will read the data
     */
    public void onEndReadPluginCustomData(MetadataPosition metadataPosition, EtfsPlugin plugin);

    // ================== MISC section ==================

    /**
     * This method is called if reading the filesystem is aborted for some reason (e.g. incompatible version or unrecoverable
     * error)
     */
    public void onReadAbort();

    /**
     * This method will be called, if a plugin threw an unhandled exception. The exception will be caught by ETFS core
     * and the custom data entry will be skipped
     * @param plugin The offending plugin
     * @param e The exception thrown
     */
    public void onUnhandledPluginException(EtfsPlugin plugin, Exception e);
}
