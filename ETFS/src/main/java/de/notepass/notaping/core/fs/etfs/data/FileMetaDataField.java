package de.notepass.notaping.core.fs.etfs.data;

public class FileMetaDataField {
    public enum FILE_METADATA_FLAG {
        /**
         * State indicating that only part of this file is on this tape, the other part is on a previous multitape
         */
        IS_MULTITAPE_SPLITTED(0x000000000001L);

        private long mask;

        FILE_METADATA_FLAG(long mask) {
            this.mask = mask;
        }

        public long getSetMask() {
            return mask;
        }

        public long getUnsetMask() {
            return mask ^ 0xFFFFFFFFFFFFL;
        }
    }

    private int fileMetadata;

    public void setFlag(FILE_METADATA_FLAG flag) {
        fileMetadata |= flag.getSetMask();
    }

    public boolean getFlag(FileRuntimeMetaDataField.RUNTIME_METADATA_FLAG flag) {
        return (fileMetadata & flag.getSetMask()) != 0;
    }

    public void unsetFlag(FILE_METADATA_FLAG flag) {
        fileMetadata &= flag.getUnsetMask();
    }

    public long getFileMetadata() {
        return fileMetadata;
    }

    public void setFileMetadata(int runtimeMetadata) {
        this.fileMetadata = runtimeMetadata;
    }
}
