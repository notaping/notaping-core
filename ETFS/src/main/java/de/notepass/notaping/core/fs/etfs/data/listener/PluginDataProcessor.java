package de.notepass.notaping.core.fs.etfs.data.listener;

import de.notepass.notaping.core.fs.etfs.data.section.HeaderSection;
import de.notepass.notaping.core.fs.etfs.data.struct.FileContent;
import de.notepass.notaping.core.fs.etfs.data.struct.FileEntry;
import de.notepass.notaping.core.fs.etfs.plugin.MetadataPosition;

import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Path;

public interface PluginDataProcessor {
    /**
     * Called when plugin custom data is read from the plugin config section
     * @param is Tape input stream
     * @param metadataPosition Position of the metadata read (In this case always {@link MetadataPosition#AFTER_HEADER}
     * @param context Header data in case plugin needs it
     */
    public void onPluginCustomDataStart(InputStream is, MetadataPosition metadataPosition, HeaderSection context);

    /**
     * Called when plugin custom data is read from the table of contents section
     * @param is Tape input stream
     * @param metadataPosition Position of the metadata read (In this case always {@link MetadataPosition#AFTER_TOC_ENTRY}
     * @param context FileEntry this custom data entry is written for
     */
    public void onPluginCustomDataStart(InputStream is, MetadataPosition metadataPosition, FileEntry context);

    /**
     * Called when plugin custom data is read from the file content section
     * @param is Tape input stream
     * @param metadataPosition Position of the metadata read (In this case always {@link MetadataPosition#AFTER_FILE_CONTENT}
     * @param context FileContent element this plugin data refers to
     * @param file Path of the file this plugin data refers to
     */
    public void onPluginCustomDataStart(InputStream is, MetadataPosition metadataPosition, FileContent context, Path file);

    /**
     * Called when plugin custom data is written to the plugin config section
     * @param os Tape output stream
     * @param metadataPosition Position of the metadata read (In this case always {@link MetadataPosition#AFTER_HEADER}
     * @param context Header data in case plugin needs it
     */
    public void onPluginCustomDataStart(OutputStream os, MetadataPosition metadataPosition, HeaderSection context);

    /**
     * Called when plugin custom data is written to the table of contents section
     * @param os Tape output stream
     * @param metadataPosition Position of the metadata read (In this case always {@link MetadataPosition#AFTER_TOC_ENTRY}
     * @param context FileEntry this custom data entry is written for
     */
    public void onPluginCustomDataStart(OutputStream os, MetadataPosition metadataPosition, FileEntry context);

    /**
     * Called when plugin custom data is written to the file content section
     * @param  os Tape output stream
     * @param metadataPosition Position of the metadata read (In this case always {@link MetadataPosition#AFTER_FILE_CONTENT}
     * @param context FileContent element this plugin data refers to
     * @param file Path of the file this plugin data refers to
     */
    public void onPluginCustomDataStart(OutputStream os, MetadataPosition metadataPosition, FileContent context, Path file);
}
