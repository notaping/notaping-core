package de.notepass.notaping.core.fs.etfs.plugin;

import de.notepass.notaping.core.fs.etfs.data.section.PluginConfigSection;
import de.notepass.notaping.core.fs.etfs.data.struct.PluginInfo;

import java.util.HashMap;
import java.util.Map;

public class PluginRegistry {
    private Map<String, EtfsPlugin> activePlugins = new HashMap<>();
    private Map<Short, String> pluginAliases = new HashMap<>();
    private Map<String, Short> reversedPluginAliases = new HashMap<>();

    public void register(EtfsPlugin plugin) {
        activePlugins.put(plugin.getID(), plugin);
    }

    public void unregister(String id) {
        activePlugins.remove(id);
    }

    public EtfsPlugin get(String id) {
        return activePlugins.get(id);
    }

    public EtfsPlugin get(short internalId) {
        return getCached(internalId);
    }

    public void buildShortIdCache(PluginConfigSection pcs) {
        for (PluginInfo pi : pcs.getPluginInfo()) {
            pluginAliases.put(pi.getInternalId(), pi.getId());
            reversedPluginAliases.put(pi.getId(), pi.getInternalId());
        }
    }

    public EtfsPlugin[] getActivePlugins() {
        return activePlugins.values().toArray(new EtfsPlugin[0]);
    }

    protected EtfsPlugin getCached(short internalId) {
        return activePlugins.get(pluginAliases.get(internalId));
    }

    public short getInternalId(String id) {
        return reversedPluginAliases.get(id);
    }

    public short getInternalId(EtfsPlugin plugin) {
        return reversedPluginAliases.get(plugin.getID());
    }
}
