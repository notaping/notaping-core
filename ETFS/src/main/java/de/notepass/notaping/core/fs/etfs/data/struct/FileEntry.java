package de.notepass.notaping.core.fs.etfs.data.struct;

import de.notepass.notaping.core.fs.etfs.data.EtfsInstance;
import de.notepass.notaping.core.fs.etfs.data.EtfsSerializableElement;
import de.notepass.notaping.core.fs.etfs.data.FileMetaDataField;
import de.notepass.notaping.core.fs.etfs.data.listener.EtfsReadProgressCallback;
import de.notepass.notaping.core.fs.etfs.data.listener.EtfsWriteProgressCallback;
import de.notepass.notaping.core.fs.etfs.plugin.EtfsPlugin;
import de.notepass.notaping.core.fs.etfs.plugin.MetadataPosition;
import de.notepass.notaping.core.fs.etfs.util.SerializeUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

/**
 * Java class representation of the "FileEntry" struct, as defined in the ETFS specification.<br/>
 * Please note: As java still lacks proper unsigned support, the data types used here are "bigger" than
 * they should be according to the specification
 */
public class FileEntry implements EtfsSerializableElement {
    // filename_size: 2 int: Size of the filename field
    private short filenameSize;

    // filename: 0:32767 string: Path and name of the file saved (normalized, see WIKI)
    private String filename;

    // file_meta: 8 int: Additional file meta data (See Metadata section for more info)
    private FileMetaDataField fileMeta = new FileMetaDataField();

    // plugin_data_length: 2 int: Number of entries in the plugin_data list
    private short pluginDataLength;

    // plugin_data: 0:32767 list<PluginCustomData>: Space for plugins to write custom data to (AFTER_FILE_DATA)
    private List<PluginCustomData> pluginData = new ArrayList<>();

    // This data isn't written to the tape, it's just here to help
    private Path targetFile;

    /**
     * filename_size: 2 int: Size of the filename field
     * @return
     */
    public short getFilenameSize() {
        return filenameSize;
    }

    /**
     * filename_size: 2 int: Size of the filename field
     * @param filenameSize
     */
    public void setFilenameSize(short filenameSize) {
        this.filenameSize = filenameSize;
    }

    /**
     * filename: 0:32767 string: Path and name of the file saved (normalized, see WIKI)
     * @return
     */
    public String getFilename() {
        return filename;
    }

    /**
     * filename: 0:32767 string: Path and name of the file saved (normalized, see WIKI)
     * @param filename
     */
    public void setFilename(String filename) {
        this.filename = filename;
    }

    /**
     * file_meta: 8 int: Additional file meta data (See Metadata section for more info)
     * @return
     */
    public FileMetaDataField getFileMeta() {
        return fileMeta;
    }

    /**
     * file_meta: 8 int: Additional file meta data (See Metadata section for more info)
     * @param fileMeta
     */
    public void setFileMeta(FileMetaDataField fileMeta) {
        this.fileMeta = fileMeta;
    }

    /**
     * plugin_data_length: 2 int: Number of entries in the plugin_data list
     * @return
     */
    public short getPluginDataLength() {
        return pluginDataLength;
    }

    /**
     * plugin_data_length: 2 int: Number of entries in the plugin_data list
     * @param pluginDataLength
     */
    public void setPluginDataLength(short pluginDataLength) {
        this.pluginDataLength = pluginDataLength;
    }

    /**
     * plugin_data: 0:32767 list<PluginCustomData>: Space for plugins to write custom data to (AFTER_FILE_DATA)
     * @return
     */
    public List<PluginCustomData> getPluginData() {
        return pluginData;
    }

    /**
     * plugin_data: 0:32767 list<PluginCustomData>: Space for plugins to write custom data to (AFTER_FILE_DATA)
     * @param pluginData
     */
    public void setPluginData(List<PluginCustomData> pluginData) {
        this.pluginData = pluginData;
    }

    /**
     * Reference to file on disk the content should be written to/read from
     * @return
     */
    public Path getTargetFile() {
        return targetFile;
    }

    /**
     * Reference to file on disk the content should be written to/read from
     * @return
     */
    public void setTargetFile(Path targetFile) {
        this.targetFile = targetFile;
    }

    @Override
    public void serialize(OutputStream os, EtfsInstance context, EtfsWriteProgressCallback callback) throws IOException {
        // Step 1: Recalculate fields
        filename = SerializeUtils.normalizeFilePath(targetFile);
        filenameSize = (short) filename.length();

        //TODO: Add multitape support

        // Step 2: Write data
        callback.onStartWriteTableOfContentsEntry(this);
        os.write(SerializeUtils.toBytes(filenameSize));
        os.write(SerializeUtils.toBytes(filename));
        os.write(SerializeUtils.toBytes(fileMeta.getFileMetadata()));
        for (EtfsPlugin plugin : context.getPluginRegistry().getActivePlugins()) {
            if (plugin.wantsToWritePluginData(MetadataPosition.AFTER_TOC_ENTRY, this)) {
                PluginCustomData pcd = new PluginCustomData(MetadataPosition.AFTER_TOC_ENTRY, this);
                pluginData.add(pcd);
            }
        }
        pluginDataLength = (short) pluginData.size();
        os.write(SerializeUtils.toBytes(pluginDataLength));
        callback.onStartWritePluginCustomDataSection(MetadataPosition.AFTER_TOC_ENTRY, pluginDataLength);
        for (PluginCustomData pcd : pluginData) {
            pcd.serialize(os, context, callback);
        }
        callback.onEndWritePluginCustomDataSection(MetadataPosition.AFTER_TOC_ENTRY);
        callback.onEndWriteTableOfContentsEntry(this);
    }

    @Override
    public void deserialize(InputStream os, long version, EtfsInstance context, EtfsReadProgressCallback callback) throws IOException {

    }
}
