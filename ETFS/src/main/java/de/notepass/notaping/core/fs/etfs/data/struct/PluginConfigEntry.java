package de.notepass.notaping.core.fs.etfs.data.struct;

import de.notepass.notaping.core.fs.etfs.data.EtfsInstance;
import de.notepass.notaping.core.fs.etfs.data.EtfsSerializableElement;
import de.notepass.notaping.core.fs.etfs.data.PluginMetaDataField;
import de.notepass.notaping.core.fs.etfs.data.listener.EtfsReadProgressCallback;
import de.notepass.notaping.core.fs.etfs.data.listener.EtfsWriteProgressCallback;
import de.notepass.notaping.core.fs.etfs.plugin.EtfsPlugin;
import de.notepass.notaping.core.fs.etfs.util.SerializeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;

import static de.notepass.notaping.core.fs.etfs.util.SerializeUtils.toBytes;

/**
 * Java class representation of the "PluginConfigEntry" struct, as defined in the ETFS specification.<br/>
 * Please note: As java still lacks proper unsigned support, the data types used here are "bigger" than
 * they should be according to the specification
 */
public class PluginConfigEntry implements EtfsSerializableElement {
    // size: 4 int: Size of the entry, excluding this field
    private int size;

    // internal_id: 2 int: Reference to the internal ID from the header section
    private short internalId;

    // The data itself is not stored here, as it may be multiple GB per plugin. The plugin will later
    // get a reference to the tape stream from which it can read data and work with it in an optimised
    // way
    // content: 2147483000 byte: Configuration file content. Advised to be string, but can be any data format

    // plugin_data_metadata: 4 int: Flags to manage problems that happened on write time (See plugin_data_metadata for more information)
    private PluginMetaDataField pluginDataMetadata = new PluginMetaDataField();

    private static final Logger LOGGER = LoggerFactory.getLogger(PluginConfigEntry.class);


    /**
     * size: 4 int: Size of the entry, excluding this field
     * @return
     */
    public int getSize() {
        return size;
    }

    /**
     * size: 4 int: Size of the entry, excluding this field
     * @param size
     */
    public void setSize(int size) {
        this.size = size;
    }

    /**
     * internal_id: 2 int: Reference to the internal ID from the header section
     * @return
     */
    public short getInternalId() {
        return internalId;
    }

    /**
     * internal_id: 2 int: Reference to the internal ID from the header section
     * @param internalId
     */
    public void setInternalId(short internalId) {
        this.internalId = internalId;
    }

    /**
     * plugin_data_metadata: 4 int: Flags to manage problems that happened on write time (See plugin_data_metadata for more information)
     * @return
     */
    public PluginMetaDataField getPluginDataMetadata() {
        return pluginDataMetadata;
    }

    /**
     * plugin_data_metadata: 4 int: Flags to manage problems that happened on write time (See plugin_data_metadata for more information)
     * @param pluginDataMetadata
     */
    public void setPluginDataMetadata(PluginMetaDataField pluginDataMetadata) {
        this.pluginDataMetadata = pluginDataMetadata;
    }

    @Override
    public void serialize(OutputStream os, EtfsInstance context, EtfsWriteProgressCallback callback) throws IOException {
        EtfsPlugin plugin = context.getPluginRegistry().get(internalId);

        callback.onStartWritePluginConfiguration(plugin);

        // Step 1: Write plugin data to temporary file, as the size needs to be known before writing to tape
        Path pcdTempFile = SerializeUtils.getTempFile();
        boolean pluginError = false;
        try (OutputStream fos = Files.newOutputStream(pcdTempFile)) {
            plugin.writeRequiredConfigForReadOperation(fos);
        } catch (Exception e) {
            Files.delete(pcdTempFile);

            if (e instanceof IOException) {
                throw e;
            } else {
                LOGGER.error("Unhandled exception in plugin \"" + plugin.getID() + "\", no config data will be written", e);
                pluginError = true;
                callback.onUnhandledPluginException(plugin, e);
                pluginDataMetadata.setFlag(PluginMetaDataField.PLUGIN_METADATA_FLAG.PLUGIN_ERROR);
            }
        }

        // Step 2: Prepare values
        if (!pluginError) {
            size = (int) Files.size(pcdTempFile);
        } else {
            size = 0;
        }

        // Step 3: Write values
        os.write(toBytes(size));
        os.write(toBytes(internalId));

        if (!pluginError) {
            try (InputStream is = Files.newInputStream(pcdTempFile)) {
                is.transferTo(os);
            } finally {
                Files.delete(pcdTempFile);
            }
        }

        os.write(toBytes(pluginDataMetadata.getPluginDataMetadata()));

        callback.onEndWritePluginConfiguration(plugin);
    }

    @Override
    public void deserialize(InputStream os, long version, EtfsInstance context, EtfsReadProgressCallback callback) throws IOException {

    }
}
