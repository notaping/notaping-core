import callback.PrintingWriteProgressCallback;
import de.notepass.notaping.core.fs.etfs.data.EtfsInstance;
import de.notepass.notaping.core.fs.etfs.data.EtfsWriteDirectionBuilder;
import org.junit.Test;
import plugin.TestPlugin;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;

public class EtfsSerializeTest {
    //@Test
    public void serialize() throws IOException {
        OutputStream os = Files.newOutputStream(Path.of("C:\\temp\\test.bin"));
        EtfsInstance e = EtfsWriteDirectionBuilder.newInstance()
                .addFile(Path.of("C:\\temp\\numbers.txt"))
                .addPlugin(new TestPlugin())
                .withLabel("TEST_TAPE")
                .build();

        e.serialize(os, new PrintingWriteProgressCallback());
    }
}
