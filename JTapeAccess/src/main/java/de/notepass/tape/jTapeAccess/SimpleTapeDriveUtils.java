package de.notepass.tape.jTapeAccess;

import de.notepass.tape.jTapeAccess.impl.windows.SimpleTapeDriveWindowsBindings;
import de.notepass.tape.jTapeAccess.model.SimpleTapeDrive;
import de.notepass.tape.jTapeAccess.model.TapeException;

import java.util.HashMap;
import java.util.Map;

public class SimpleTapeDriveUtils {
    private static Map<String, SimpleTapeDrive> instances = new HashMap<>();

    /**
     * Returns a plattform-specif implementation of the SimpleTapeDrive class
     * @param tapePath Path to the tape file to access the tape (e.g. \\.\TAPE0 on windows)
     * @return Plattform-specif implementation of the SimpleTapeDrive class
     * @throws TapeException If the drive cannot be correctly accessed the underlying system APIs, or if no implementation for the currently running system exists
     */
    public static synchronized SimpleTapeDrive getTapeDriveInstance(String tapePath) throws TapeException {
        SimpleTapeDrive instance = instances.get(tapePath);

        if (instance == null) {
            if (isWindowsOs()) {
                instance = new SimpleTapeDriveWindowsBindings(tapePath);
            }

            if (instance != null) {
                instances.put(tapePath, instance);
            } else {
                throw new TapeException(TapeException.ErrorCode.NO_NATIVE_IMPL_AVAILABLE, "No bindings for accessing the tape drive are available for this OS ("+System.getProperty("os.name")+")");
            }
        }

        return instance;
    }

    public static boolean isWindowsOs() {
        return System.getProperty("os.name").toLowerCase().contains("windows");
    }

    public static boolean isLinuxOs() {
        return System.getProperty("os.name").toLowerCase().contains("linux");
    }

    public static boolean isMacOs() {
        return System.getProperty("os.name").toLowerCase().contains("mac");
    }
}
