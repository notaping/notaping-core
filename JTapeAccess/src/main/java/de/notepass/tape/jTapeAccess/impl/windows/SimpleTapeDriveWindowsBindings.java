package de.notepass.tape.jTapeAccess.impl.windows;

import com.sun.jna.platform.win32.WinDef;
import com.sun.jna.platform.win32.WinError;
import com.sun.jna.platform.win32.WinNT;
import com.sun.jna.ptr.IntByReference;
import de.notepass.tape.jTapeAccess.model.*;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static de.notepass.tape.jTapeAccess.impl.windows.Kernel32WithTape.*;

public class SimpleTapeDriveWindowsBindings implements SimpleTapeDrive {
    private static final Kernel32WithTape kernel32 = Kernel32WithTape.INSTANCE;
    private WinNT.HANDLE tapeHandle;
    public ParsedTapeDriveCapabilities tapeDriveCapabilities;
    private String tapeDriveFile;

    public SimpleTapeDriveWindowsBindings(String tapeDriveFile) throws TapeException {
        this.tapeDriveFile = tapeDriveFile;

        tapeHandle = kernel32.CreateFile(tapeDriveFile,
                WinNT.GENERIC_READ | WinNT.GENERIC_WRITE, /* Read and write access for tape drive */
                0, /* Do not allow other processes to access the tape drive */
                null, /* Unused for tape drives */
                WinNT.OPEN_EXISTING, /* Open an existing file */
                0, /* Do not set any attributes */
                null /* Not needed */
        );

        if (tapeHandle.toString().endsWith("@0xffffffffffffffff") || tapeHandle.toString().endsWith("@0xffffffff")) {
            throw new TapeException(TapeException.ErrorCode.DEVICE_NOT_FOUND, "Could not open tape drive " + tapeDriveFile + ". Is it connected and is the application running with administrator privileges?");
        }

        Kernel32WithTape.TAPE_GET_DRIVE_PARAMETERS.ByReference capabilitiesRef = new Kernel32WithTape.TAPE_GET_DRIVE_PARAMETERS.ByReference();
        checkError(
                kernel32.GetTapeParameters(
                        tapeHandle,
                        new WinDef.DWORD(GET_TAPE_DRIVE_INFORMATION), /* Get tape drive info instead of tape info*/
                        new WinDef.DWORDByReference(new WinDef.DWORD(capabilitiesRef.size())),
                        capabilitiesRef
                )
        );

        tapeDriveCapabilities = new ParsedTapeDriveCapabilities(getTapeDriveInfo());

        setMediumBlockSize(64 * 1024);
    }

    @Override
    public void loadTape() throws TapeException {
        checkError(kernel32.PrepareTape(tapeHandle, new WinDef.DWORD(TAPE_LOAD), new WinDef.BOOL(false)));
    }

    @Override
    public void ejectTape() throws TapeException {
        checkError(kernel32.PrepareTape(tapeHandle, new WinDef.DWORD(TAPE_UNLOAD), new WinDef.BOOL(false)));
    }

    @Override
    public boolean isReady() {
        return getErrors() == null;
    }

    @Override
    public TapeException getErrors() {
        return ExceptionMapper.map(kernel32.GetTapeStatus(tapeHandle).longValue());
    }

    @Override
    public boolean isReadable() {
        return isMediaPresent();
    }

    @Override
    public boolean isWritable() throws TapeException {
        return getTapeInfo().isWriteProtected();
    }

    @Override
    public long getTapePosition() throws TapeException {
        WinDef.DWORDByReference lpdwPartition = new WinDef.DWORDByReference(new DWORD(0));
        WinDef.DWORDByReference lpdwOffsetLow = new WinDef.DWORDByReference();
        WinDef.DWORDByReference lpdwOffsetHigh = new WinDef.DWORDByReference();
        checkError(kernel32.GetTapePosition(tapeHandle, new WinDef.DWORD(TAPE_ABSOLUTE_POSITION), lpdwPartition, lpdwOffsetLow, lpdwOffsetHigh));

        return (lpdwOffsetHigh.getValue().longValue() << 32) | lpdwOffsetLow.getValue().longValue();
    }

    @Override
    public void setTapePosition(long position) throws TapeException {
        if (position != 0) {
            long low = position & 0x00000000FFFFFFFF;
            long high = position >> 32;
            checkError(kernel32.SetTapePosition(tapeHandle, new WinDef.DWORD(TAPE_ABSOLUTE_BLOCK), new WinDef.DWORD(0L), new WinDef.DWORD(low), new WinDef.DWORD(high), new WinDef.BOOL(false)));
        } else {
            checkError(kernel32.SetTapePosition(tapeHandle, new WinDef.DWORD(TAPE_REWIND), new WinDef.DWORD(0L), new WinDef.DWORD(0), new WinDef.DWORD(0), new WinDef.BOOL(false)));
        }
    }

    //TODO: Test (Insufficient documentation)
    @Override
    public void forwardToNextFilemark() throws TapeException {
        checkError(kernel32.SetTapePosition(tapeHandle, new WinDef.DWORD(TAPE_SPACE_FILEMARKS), new WinDef.DWORD(0L), new WinDef.DWORD(1), new WinDef.DWORD(0), new WinDef.BOOL(false)));
    }

    //TODO: Test (Insufficient documentation)
    @Override
    public void reverseToPreviousFilemark() throws TapeException {
        checkError(kernel32.SetTapePosition(tapeHandle, new WinDef.DWORD(TAPE_SPACE_FILEMARKS), new WinDef.DWORD(0L), new WinDef.DWORD(0), new WinDef.DWORD(1), new WinDef.BOOL(false)));
    }

    @Override
    public DriveCapabilities[] getDriveCapabilities() {
        boolean ecc = tapeDriveCapabilities.TAPE_DRIVE_ECC;
        boolean compression = tapeDriveCapabilities.TAPE_DRIVE_COMPRESSION;
        boolean padding = tapeDriveCapabilities.TAPE_DRIVE_PADDING;
        List<DriveCapabilities> driveCapabilities = new ArrayList<>(5);
        if (ecc) {
            driveCapabilities.add(DriveCapabilities.ECC);
        }

        if (compression) {
            driveCapabilities.add(DriveCapabilities.COMPRESSION);
        }

        if (padding) {
            driveCapabilities.add(DriveCapabilities.DATA_PADDING);
        }

        return driveCapabilities.toArray(new DriveCapabilities[0]);
    }

    @Override
    public void enableDriveCapability(DriveCapabilities... driveCapabilities) throws TapeException {
        Kernel32WithTape.TAPE_SET_DRIVE_PARAMETERS.ByReference driveParameters = new Kernel32WithTape.TAPE_SET_DRIVE_PARAMETERS.ByReference();
        for (DriveCapabilities driveCapability : driveCapabilities) {
            switch (driveCapability) {
                case ECC:
                    driveParameters.ECC = new WinDef.BOOL(true);
                    break;
                case COMPRESSION:
                    driveParameters.Compression = new WinDef.BOOL(true);
                    break;
                case DATA_PADDING:
                    driveParameters.DataPadding = new BOOL(true);
            }
        }


        checkError(kernel32.SetTapeParameters(tapeHandle, new WinDef.DWORD(SET_TAPE_DRIVE_INFORMATION), driveParameters));
    }

    @Override
    public void disableDriveCapability(DriveCapabilities... driveCapabilities) throws TapeException {
        Kernel32WithTape.TAPE_SET_DRIVE_PARAMETERS.ByReference driveParameters = new Kernel32WithTape.TAPE_SET_DRIVE_PARAMETERS.ByReference();
        for (DriveCapabilities driveCapability : driveCapabilities) {
            switch (driveCapability) {
                case ECC:
                    driveParameters.ECC = new WinDef.BOOL(false);
                    break;
                case COMPRESSION:
                    driveParameters.Compression = new WinDef.BOOL(false);
                    break;
                case DATA_PADDING:
                    driveParameters.DataPadding = new BOOL(false);
            }
        }

        checkError(kernel32.SetTapeParameters(tapeHandle, new DWORD(SET_TAPE_DRIVE_INFORMATION), driveParameters));
    }

    @Override
    public void setMediumBlockSize(long blockSizeBytes) throws TapeException {
        Kernel32WithTape.TAPE_SET_MEDIA_PARAMETERS.ByReference mediaParameters = new Kernel32WithTape.TAPE_SET_MEDIA_PARAMETERS.ByReference();

        mediaParameters.BlockSize = new DWORD(blockSizeBytes);

        checkError(kernel32.SetTapeParameters(tapeHandle, new WinDef.DWORD(SET_TAPE_MEDIA_INFORMATION), mediaParameters));
    }

    @Override
    public int readBytes(byte[] buffer, int offset, int numBytes) throws TapeException {
        if (offset == 0 && buffer.length == numBytes) {
            return readBytes(buffer);
        }

        byte[] subArray = new byte[numBytes];

        int readBytes = readBytes(subArray);
        System.arraycopy(subArray, 0, buffer, offset, numBytes);

        return readBytes;
    }

    @Override
    public int writeBytes(byte[] buffer, int offset, int numBytes) throws TapeException {
        IntByReference writtenBytes = new IntByReference(-1);
        byte[] rangedCopy = Arrays.copyOfRange(buffer, offset, numBytes + offset);
        return writeBytes(rangedCopy);
    }

    @Override
    public int readBytes(byte[] buffer) throws TapeException {
        if (buffer.length == 0) {
            return 0;
        }

        DWORDByReference readBytesDw = new DWORDByReference(new DWORD(-1L));
        IntByReference readBytes = new IntByReference(-1);
        checkError(kernel32.ReadFile(tapeHandle, buffer, buffer.length, readBytes, null));
        return readBytes.getValue();
    }

    @Override
    public int writeBytes(byte[] buffer) throws TapeException {
        //TODO: Throw exception
        if (buffer.length % getTapeInfo().getBlockSizeBytes() != 0)
            System.out.printf("[WARN] Trying to write data to the tape with a wrong block size. Data can only be " +
                    "written in multiples of %dKiB, but it was tried to write %dKiB. This will almost certainly fail " +
                    "with error 83!%n", (getTapeInfo().getBlockSizeBytes()/1024), (buffer.length/1024));
        IntByReference writtenBytes = new IntByReference(-1);
        checkError(kernel32.WriteFile(tapeHandle, buffer, buffer.length, writtenBytes, null));
        return writtenBytes.getValue();
    }

    @Override
    public void writeEofMark(int amount) throws TapeException {
        checkError(kernel32.WriteTapemark(tapeHandle, new WinDef.DWORD(TAPE_FILEMARKS), new WinDef.DWORD(amount), new WinDef.BOOL(false)));
    }

    @Override
    public OutputStream getOutputStream() throws TapeException {
        //TODO: Make single instance
        return new TapeOutputStream(this);
    }

    @Override
    public InputStream getInputStream() throws TapeException {
        //TODO: Make single instance
        //return new TapeInputStream(this);
        return new TapeInputStream(this);
    }

    @Override
    public void flushCaches() {
        //TODO
    }

    @Override
    public boolean isMediaPresent() {
        WinDef.DWORD driveStatus = kernel32.GetTapeStatus(tapeHandle);
        return driveStatus.intValue() != WinError.ERROR_NO_MEDIA_IN_DRIVE;
    }

    @Override
    public TapeInfo getTapeInfo() throws TapeException {
        TAPE_GET_MEDIA_PARAMETERS tapeInfo = getInsertedTapeInfo();
        return new TapeInfo(
                tapeInfo.Remaining.getValue(),
                tapeInfo.Capacity.getValue() - tapeInfo.Remaining.getValue(),
                tapeInfo.Capacity.getValue(),
                tapeInfo.WriteProtected.booleanValue(),
                tapeInfo.BlockSize.longValue()
        );
    }

    @Override
    public void partitionTape() throws TapeException {
        kernel32.CreateTapePartition(tapeHandle, new DWORD(TAPE_FIXED_PARTITIONS), new DWORD(1), new DWORD(-1));
    }

    protected Kernel32WithTape.TAPE_GET_MEDIA_PARAMETERS getInsertedTapeInfo() throws TapeException {
        Kernel32WithTape.TAPE_GET_MEDIA_PARAMETERS.ByReference mediaInfo = new Kernel32WithTape.TAPE_GET_MEDIA_PARAMETERS.ByReference();
        checkError(kernel32.GetTapeParameters(tapeHandle, new WinDef.DWORD(GET_TAPE_MEDIA_INFORMATION), new WinDef.DWORDByReference(new WinDef.DWORD(mediaInfo.size())), mediaInfo));
        return mediaInfo;
    }

    protected Kernel32WithTape.TAPE_GET_DRIVE_PARAMETERS getTapeDriveInfo() throws TapeException {
        Kernel32WithTape.TAPE_GET_DRIVE_PARAMETERS.ByReference mediaInfo = new Kernel32WithTape.TAPE_GET_DRIVE_PARAMETERS.ByReference();
        checkError(kernel32.GetTapeParameters(tapeHandle, new WinDef.DWORD(GET_TAPE_DRIVE_INFORMATION), new WinDef.DWORDByReference(new WinDef.DWORD(mediaInfo.size())), mediaInfo));
        return mediaInfo;
    }

    protected void checkError(DWORD resultCode) throws TapeException {
        TapeException mappedException = ExceptionMapper.map(resultCode.longValue());
        if (mappedException != null) {
            throw mappedException;
        }
    }

    protected void checkError(boolean result) throws TapeException {
        if (!result) {
            TapeException mappedException = ExceptionMapper.map(kernel32.GetLastError());

            if (mappedException != null) {
                throw mappedException;
            }
        }
    }

    public String getTapeDriveFile() {
        return tapeDriveFile;
    }
}
