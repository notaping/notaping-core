package de.notepass.notaping.core.fs.etfs.data.section;

import de.notepass.notaping.core.fs.etfs.data.EtfsInstance;
import de.notepass.notaping.core.fs.etfs.data.EtfsSerializableElement;
import de.notepass.notaping.core.fs.etfs.data.listener.EtfsReadProgressCallback;
import de.notepass.notaping.core.fs.etfs.data.listener.EtfsWriteProgressCallback;
import de.notepass.notaping.core.fs.etfs.data.struct.PluginInfo;
import de.notepass.notaping.core.fs.etfs.util.SerializeUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.UUID;

/**
 * Java class representation of the "Header" section, as defined in the ETFS specification.<br/>
 * Please note: As java still lacks proper unsigned support, the data types used here are "bigger" than
 * they should be according to the specification
 */
public class HeaderSection implements EtfsSerializableElement {
    // magic: 8 string: "Magic string" to indicate ETFS tape. Is always "//ETFS//"
    public static final String MAGIC = "//ETFS//";

    // version: 4 int: Version of the ETFS core used to create the tape
    private int version;

    // min_version: 4 int: Minimum ETFS core version which is necessary to read this tape
    private int minVersion;

    // allow_multitape: 1 int: Boolean value, indicating if this tape can be seen as a possible multitape volume (See WIKI)
    private boolean allowMultitape;

    // multitape_index: 2 int: The index of this tape in a multitape volume. Starts at 0 for the first tape.
    private short multitapeIndex;

    // label_size: 2 int: Size of the tape label in bytes
    private short labelSize;

    // label: 0:32767 string: A user-defined, human readable, label for the tape (Or some other stuff, as long as it can be stringed. I'm a doc,not a cop)
    private String label;

    // volume_id_size: 1 int: Size of the tape volume_id in bytes
    private byte volumeIdSize;

    // volume_id: 0:126 string: ID of the volume, for use with multitape volumes
    private UUID volumeId;

    /**
     * version: 4 int: Version of the ETFS core used to create the tape
     * @return
     */
    public int getVersion() {
        return version;
    }

    /**
     * version: 4 int: Version of the ETFS core used to create the tape
     * @param version
     */
    public void setVersion(int version) {
        this.version = version;
    }

    /**
     * min_version: 4 int: Minimum ETFS core version which is necessary to read this tape
     * @return
     */
    public int getMinVersion() {
        return minVersion;
    }

    /**
     * min_version: 4 int: Minimum ETFS core version which is necessary to read this tape
     * @param minVersion
     */
    public void setMinVersion(int minVersion) {
        this.minVersion = minVersion;
    }

    /**
     * allow_multitape: 1 int: Boolean value, indicating if this tape can be seen as a possible multitape volume (See WIKI)
     * @return
     */
    public boolean isAllowMultitape() {
        return allowMultitape;
    }

    /**
     * allow_multitape: 1 int: Boolean value, indicating if this tape can be seen as a possible multitape volume (See WIKI)
     * @param allowMultitape
     */
    public void setAllowMultitape(boolean allowMultitape) {
        this.allowMultitape = allowMultitape;
    }

    /**
     * multitape_index: 2 int: The index of this tape in a multitape volume. Starts at 0 for the first tape.
     * @return
     */
    public short getMultitapeIndex() {
        return multitapeIndex;
    }

    /**
     * multitape_index: 2 int: The index of this tape in a multitape volume. Starts at 0 for the first tape.
     * @param multitapeIndex
     */
    public void setMultitapeIndex(short multitapeIndex) {
        this.multitapeIndex = multitapeIndex;
    }

    /**
     * label_size: 2 int: Size of the tape label in bytes
     * @return
     */
    public short getLabelSize() {
        return labelSize;
    }

    /**
     * label_size: 2 int: Size of the tape label in bytes
     * @param labelSize
     */
    public void setLabelSize(short labelSize) {
        this.labelSize = labelSize;
    }

    /**
     * label: 0:32767 string: A user-defined, human readable, label for the tape (Or some other stuff, as long as it can be stringed. I'm a doc,not a cop)
     * @return
     */
    public String getLabel() {
        return label;
    }

    /**
     * label: 0:32767 string: A user-defined, human readable, label for the tape (Or some other stuff, as long as it can be stringed. I'm a doc,not a cop)
     * @param label
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * volume_id_size: 1 int: Size of the tape volume_id in bytes
     * @return
     */
    public byte getVolumeIdSize() {
        return volumeIdSize;
    }

    /**
     * volume_id_size: 1 int: Size of the tape volume_id in bytes
     * @param volumeIdSize
     */
    public void setVolumeIdSize(byte volumeIdSize) {
        this.volumeIdSize = volumeIdSize;
    }

    /**
     * volume_id: 0:126 string: ID of the volume, for use with multitape volumes
     * @return
     */
    public UUID getVolumeId() {
        return volumeId;
    }

    /**
     * volume_id: 0:126 string: ID of the volume, for use with multitape volumes
     * @param volumeId
     */
    public void setVolumeId(UUID volumeId) {
        this.volumeId = volumeId;
    }

    @Override
    public void serialize(OutputStream os, EtfsInstance context, EtfsWriteProgressCallback callback) throws IOException {
        String volumeId = this.volumeId.toString();
        labelSize = (short) SerializeUtils.sizeof(label);
        labelSize = (short) SerializeUtils.sizeof(volumeId);

        callback.onStartWriteHeaderSection(this);
        os.write(SerializeUtils.toBytes(version));
        os.write(SerializeUtils.toBytes(minVersion));
        os.write(SerializeUtils.toBytes(allowMultitape));
        os.write(SerializeUtils.toBytes(multitapeIndex));
        os.write(SerializeUtils.toBytes(volumeIdSize));
        os.write(SerializeUtils.toBytes(volumeId));
        os.write(SerializeUtils.toBytes(labelSize));
        os.write(SerializeUtils.toBytes(label));
        callback.onEndWriteHeaderSection(this);
    }

    @Override
    public void deserialize(InputStream os, long version, EtfsInstance context, EtfsReadProgressCallback callback) throws IOException {

    }
}
