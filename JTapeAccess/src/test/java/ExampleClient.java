import com.sun.jna.platform.win32.Kernel32;
import com.sun.jna.platform.win32.WinNT;
import com.sun.jna.ptr.IntByReference;
import de.notepass.tape.jTapeAccess.SimpleTapeDriveUtils;
import de.notepass.tape.jTapeAccess.impl.mock.FileMockedTapeDrive;
import de.notepass.tape.jTapeAccess.impl.windows.Kernel32WithTape;
import de.notepass.tape.jTapeAccess.impl.windows.SimpleTapeDriveWindowsBindings;
import de.notepass.tape.jTapeAccess.model.BufferedTapeOutputStream;
import de.notepass.tape.jTapeAccess.model.DriveCapabilities;
import de.notepass.tape.jTapeAccess.model.SimpleTapeDrive;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Random;

public class ExampleClient {
    public static void main(String[] args) throws IOException {
        System.out.println(Long.toBinaryString(1L));
        ByteBuffer bb = ByteBuffer.allocate(Long.BYTES);
        bb.putLong(1L);
        byte[] l = bb.array();
        System.out.println(Arrays.toString(l));
        bb = ByteBuffer.allocate(Long.BYTES);
        bb.put(l);
        long ll = bb.getLong(0);
        System.out.println(ll);

        if (1 == 1)
            return;
        OutputStream os = new FileOutputStream(new File("C:\\temp\\tapeemu.bin"));
        BufferedTapeOutputStream tos = new BufferedTapeOutputStream(os, 64);
        String data = "1";
        for (int i=0;i<100;i++) {
            System.out.printf("%d of 100%n", i);
            tos.write(data.getBytes());
        }

        tos.flush();

        byte[] bigDataSet = new byte[230];
        Arrays.fill(bigDataSet, (byte) '2');

        tos.write(bigDataSet);
        tos.flush();


        if (1 == 1)
            return;
        Kernel32 kernel32 = Kernel32.INSTANCE;
        WinNT.HANDLE fileHandle = kernel32.CreateFile("C:\\Users\\kim\\Downloads\\update.zip",
                WinNT.GENERIC_READ | WinNT.GENERIC_WRITE, /* Read and write access for tape drive */
                0, /* Do not allow other processes to access the tape drive */
                null, /* Unused for tape drives */
                WinNT.OPEN_EXISTING, /* Open an existing file */
                0, /* Do not set any attributes */
                null /* Not needed */
        );
        if (fileHandle.toString().endsWith("@0xffffffffffffffff") || fileHandle.toString().endsWith("@0xffffffff")) {
            System.err.println("[ERROR] Could not create file handle");
            return;
        }

        byte[] buf = new byte[50 * 1024 * 1024];

        IntByReference readBytes = new IntByReference();
        boolean result = kernel32.ReadFile(fileHandle, buf, buf.length, readBytes, null);
        if (!result) {
            System.err.println("[WARN] Error was thrown while trying to read file: "+kernel32.GetLastError());
            return;
        }

        if (readBytes.getValue() < buf.length) {
            System.err.printf("[WARN] Only read %d bytes from the file instead of the requested %d bytes!%n", readBytes.getValue(), buf.length);
            return;
        }

        System.out.println("Everything is OK!");

        System.exit(0);

        SimpleTapeDrive tapeDrive = SimpleTapeDriveUtils.getTapeDriveInstance("\\\\.\\TAPE0");

        if (!tapeDrive.isMediaPresent()) {
            System.out.println("No tape in tape drive. Loading it in...");
            tapeDrive.loadTape();
        }

        if (!tapeDrive.isMediaPresent()) {
            System.err.println("No media could be loaded!");
            return;
        }

        if (tapeDrive.getTapePosition() != 0) {
            System.out.println("Tape is not at beginning. Rewinding");
            tapeDrive.setTapePosition(0);
        }

        /*
        DriveCapabilities[] driveCapabilities = tapeDrive.getDriveCapabilities();
        if (Arrays.stream(driveCapabilities).anyMatch(e -> e == DriveCapabilities.COMPRESSION)) {
            System.out.println("Drive supports compression. Enabling it.");
            tapeDrive.enableDriveCapability(DriveCapabilities.COMPRESSION);
        }
        */

        System.out.println("Enabling/Disabeling default drive capabilities");
        tapeDrive.enableDriveCapability(DriveCapabilities.COMPRESSION, DriveCapabilities.ECC);
        tapeDrive.disableDriveCapability(DriveCapabilities.DATA_PADDING);

        int numGbWrite = 5;
        int bufferSizeMb = 100;
        long randomSeed = new Random().nextLong();
        System.out.println("Ready. Writing "+numGbWrite+"GB of random data");
        Random random = new Random(randomSeed);
        Random checkRandom = new Random(randomSeed);
        byte[] buffer = new byte[bufferSizeMb * 1024 * 1024];
        for (int i=0;i<((numGbWrite * 1024)/bufferSizeMb);i++) {
            random.nextBytes(buffer);
            tapeDrive.writeBytes(buffer);
            System.out.println("Wrote "+(i*bufferSizeMb)+"MB of "+numGbWrite+"GB");
        }

        System.out.println("Done. Rewinding and checking.");

        tapeDrive.setTapePosition(0);

        byte[] validationBuffer = new byte[buffer.length];
        for (int i=0;i<((numGbWrite * 1024)/bufferSizeMb);i++) {
            checkRandom.nextBytes(validationBuffer);
            tapeDrive.readBytes(buffer);

            if (Arrays.equals(buffer, validationBuffer)) {
                System.out.println("Validating 100MB ("+(i*bufferSizeMb)+"MB of "+numGbWrite+"GB total): OK");
            } else {
                System.out.println("Validating 100MB ("+(i*bufferSizeMb)+"MB of "+numGbWrite+"GB total): ERR");
            }
        }

        System.out.println("Rewinding tape");
        tapeDrive.setTapePosition(0);

        System.out.println("Ejecting tape");
        tapeDrive.ejectTape();
    }
}
