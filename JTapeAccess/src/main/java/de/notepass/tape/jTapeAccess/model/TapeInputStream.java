package de.notepass.tape.jTapeAccess.model;

import de.notepass.tape.jTapeAccess.model.SimpleTapeDrive;

import java.io.IOException;
import java.io.InputStream;

/**
 * Extremely minimalistic InputStream which uses the tape drive interface for communications
 */
public class TapeInputStream extends InputStream {
    private final SimpleTapeDrive tapeDevice;

    public TapeInputStream(SimpleTapeDrive td) {
        tapeDevice = td;
    }

    @Override
    public int read() throws IOException {
        byte[] bytes = new byte[1];
        tapeDevice.readBytes(bytes);
        return bytes[0];
    }

    /**
     * Skips the given amount of bytes by setting the position of the tape.
     * This will put the drive in high speed transfer mode and should be faster than reading bytes directly<br/>
     * <b>Please note</b>: Skipping is only possible in multiples of the tape block size. Use a BufferedTapeInputStream
     * for jumping misc sizes.
     * @param n Number of bytes to skip
     * @return
     * @throws IOException
     */
    @Override
    public long skip(long n) throws IOException {
        //TODO: Test
        long blockSize = tapeDevice.getTapeInfo().getBlockSizeBytes();

        // Initialize with current pos, as setting the position is absolute, not relative
        long posSkip = tapeDevice.getTapePosition();

        if (n % blockSize != 0) {
            throw new IOException(String.format("Invalid jump size. Can only jump in multiples of %d bytes, but tried to jump %d bytes (Use a BufferedTapeInputStream instead to do this)%n", blockSize, n));
        }

        posSkip += (n / blockSize);

        // Remove one index, as we're currently at the start of an block (As only complete blocks can be read)
        // Else we would skip an additional block
        tapeDevice.setTapePosition(posSkip - 1);

        // The drive should always skip the correct amount of bytes with the data given. The only error case should be
        // that the end of the tape would be reached, which would lead to an error on the next read access
        return n;
    }

    @Override
    public int read(byte[] b) throws IOException {
        return tapeDevice.readBytes(b);
    }

    @Override
    public int read(byte[] b, int off, int len) throws IOException {
        return tapeDevice.readBytes(b, off, len);
    }
}
