package de.notepass.tape.jTapeAccess.model;

public class TapeInfo {
    private long remainingSpace;
    private long usedSpace;
    private long totalSpace;
    private boolean writeProtected;
    private long blockSizeBytes;

    public TapeInfo(long remainingSpace, long usedSpace, long totalSpace, boolean writeProtected, long blockSizeBytes) {
        this.remainingSpace = remainingSpace;
        this.usedSpace = usedSpace;
        this.totalSpace = totalSpace;
        this.writeProtected = writeProtected;
        this.blockSizeBytes = blockSizeBytes;
    }

    /**
     * Remaining space on tape in bytes
     * @return
     */
    public long getRemainingSpace() {
        return remainingSpace;
    }

    /**
     * Used space on tape in bytes
     * @return
     */
    public long getUsedSpace() {
        return usedSpace;
    }

    /**
     * Total space on tape in bytes
     * @return
     */
    public long getTotalSpace() {
        return totalSpace;
    }

    /**
     * Checks if write protection is enabled
     * @return
     */
    public boolean isWriteProtected() {
        return writeProtected;
    }

    /**
     * Returns the block size of the medium in bytes
     * @return
     */
    public long getBlockSizeBytes() {
        return blockSizeBytes;
    }
}
