package de.notepass.notaping.core.fs.etfs.data.listener;

import de.notepass.notaping.core.fs.etfs.data.section.HeaderSection;
import de.notepass.notaping.core.fs.etfs.data.section.PluginConfigSection;
import de.notepass.notaping.core.fs.etfs.data.section.TableOfContentSection;
import de.notepass.notaping.core.fs.etfs.data.struct.FileEntry;
import de.notepass.notaping.core.fs.etfs.plugin.EtfsPlugin;
import de.notepass.notaping.core.fs.etfs.plugin.MetadataPosition;

import java.io.IOException;
import java.nio.file.Path;

public interface EtfsWriteProgressCallback {
    /**
     * The amount of bytes written after which {@link #onFileProgress(Path, long, long)} is triggered
     */
    public long reportByteSize = 64*1024;

    // ================== Header section ==================

    /**
     * This method is called before parsing the header section
     * @param headerSection HeaderSection which will be written
     */
    public void onStartWriteHeaderSection(HeaderSection headerSection);

    /**
     * This method is called after the header section was written
     * @param headerSection HeaderSection which was written
     */
    public void onEndWriteHeaderSection(HeaderSection headerSection);

    // ================== Plugin config section ==================

    /**
     * This method is called before the "Plugin config section" is written
     * @param pluginConfigSection PluginConfigSection which will be written
     */
    public void onStartWritePluginConfigSection(PluginConfigSection pluginConfigSection);

    /**
     * This method is called after the "Plugin config section" is written
     * @param pluginConfigSection Parsed pluginconfig section data
     */
    public void onEndWritePluginConfigSection(PluginConfigSection pluginConfigSection);

    /**
     * This method is called before the short-ID data of a plugin is written to the tape
     * @param plugin Parsed plugin data
     */
    public void onStartWritePluginInfo(EtfsPlugin plugin);

    /**
     * This method is called after the short-ID data of a plugin is written to the tape
     * @param plugin Parsed plugin data
     */
    public void onEndWritePluginInfo(EtfsPlugin plugin);

    /**
     * This method is called before the custom config of a plugin is written to the tape
     * @param plugin Plugin writing the data
     */
    public void onStartWritePluginConfiguration(EtfsPlugin plugin);

    /**
     * This method is called after the custom config of a plugin is written to the tape
     * @param plugin Plugin writing the data
     */
    public void onEndWritePluginConfiguration(EtfsPlugin plugin);

    // ================== TOC section ==================

    /**
     * This method is called before the table of contents section is written
     * @param tableOfContentSection The TOC section which will be written
     */
    public void onStartWriteTableOfContentSection(TableOfContentSection tableOfContentSection);

    /**
     * This method is called before a file entry is written to the TOC
     * @param entry The parsed file entry
     */
    public void onStartWriteTableOfContentsEntry(FileEntry entry);

    /**
     * This method is called after a file entry was written to the TOC
     * @param entry The parsed file entry
     */
    public void onEndWriteTableOfContentsEntry(FileEntry entry);

    /**
     * This method is called after the table of contents section is parsed
     * @param tableOfContentSection The parsed section data
     */
    public void onEndWriteTableOfContentSection(TableOfContentSection tableOfContentSection);

    // ================== File section ==================

    /**
     * This method is called before the file section is written
     * @param entries Number of files in the file section. Please note, that they don't have to be on the same tape,
     *                as multitape volumes exist
     */
    public void onStartWriteFileSection(long entries);

    /**
     * This method is called after the file section is written
     */
    public void onEndWriteFileSection();

    /**
     * This method is called before processing a file
     * @param target Reference to the file target
     * @param fileSize Size of the file
     */
    public void onStartWriteFile(Path target, long fileSize);

    /**
     * This method is called after processing a file
     * @param target Reference to the file target
     * @param fileSize Size of the file
     */
    public void onEndWriteFile(Path target, long fileSize);

    /**
     * This method is called if a file on the tape cannot be processed (e.g. because it couldn't be written to the tape)
     * @param target File which should have been written to tape
     * @param e Exception thrown
     */
    public void onUnreadableFile(Path target, IOException e);

    /**
     * This method is called while processing files. It is called after {@link #reportByteSize} bytes where written to the tape
     * @param target Reference to the file target
     * @param currentPosition Amount of bytes already written
     * @param fileSize Overall size of the file
     */
    public void onFileProgress(Path target, long currentPosition, long fileSize);

    // ================== Plugin custom data section ==================

    /**
     * This method is called before the plugin custom data block (=list) at a specific position is written
     * @param metadataPosition Which position the metadata is at
     * @param totalEntries The total amount of entries in the list
     */
    public void onStartWritePluginCustomDataSection(MetadataPosition metadataPosition, short totalEntries);

    /**
     * This method is called after the plugin custom data block (=list) at a specific position is written
     * @param metadataPosition Which position the metadata is at
     */
    public void onEndWritePluginCustomDataSection(MetadataPosition metadataPosition);

    /**
     * This method is called before a single plugin custom data entry is written
     * @param metadataPosition Which position the metadata is at
     * @param plugin The plugin which will write the data
     */
    public void onStartWritePluginCustomData(MetadataPosition metadataPosition, EtfsPlugin plugin);

    /**
     * This method is called after a single plugin custom data entry is written
     * @param metadataPosition Which position the metadata is at
     * @param plugin The plugin which will write the data
     */
    public void onEndWritePluginCustomData(MetadataPosition metadataPosition, EtfsPlugin plugin);

    // ================== MISC section ==================

    /**
     * This method is called if writing the filesystem is aborted for some reason (e.g. incompatible version or unrecoverable
     * error)
     */
    public void onWriteAbort();

    /**
     * This method will be called, if a plugin threw an unhandled exception. The exception will be caught by ETFS core
     * @param plugin The offending plugin
     * @param e The exception thrown
     */
    public void onUnhandledPluginException(EtfsPlugin plugin, Exception e);
}
