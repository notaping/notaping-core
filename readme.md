# The notaping project
The notaping project aims to provide software to save files to tape. In theory every type of tape solution
should be supported (As long as the native bindings used support it), but it mainly aims at LTO storage.  
The framework given with the core components allows for files to be saved onto an extendable file system
(ETFS), which can take additional data via the loading of plugins. This means, that everybody can customize
what data is written and also create plugins themself without worriing about creating the data structure around
it.  
The second target of the project is it, to provide CLI and GUI interfaces to store files (e.g. for
archiving them outside of HDD/SDD storage) on tape, or create backups.  
This repository only contains the core components, which are more developer focused. As soon as GUI and CLI
interfaces are available, they will be linked to in this repository.  
Following you will find more information on the core parts of the project

## JTapeAccess
JTapeAccess aims to provide access to tape drives inside java. To accomplish this, native OS methods are
accessed via JNA and put into an abstract representation, so that they can be used without knowledge about the
native methods.  
Currently, JTapeAccess only supports Windows systems, but Linux support is the next planned step. As I do not
own a MacOS system, no MacOS support is currently planned (But feel free to contribute it if you have the possibility).  

*Please note*: The JTapeAccess subproject is still in alpha stage. That means that there might be breaking changes
made without notice.

## ETFS
ETFS (the *E*xtendable *T*ape *F*ile *S*ystem) is the filesystem used to store data on the tape. The main
functionality of ETFS is its extendability via plugins, allowing custom data to be written on a per-file
basis. This also allows extending the functionality of ETFS without the need to change the specification
of the file system. Because of this, a stock version of ETFS will only write the absolut necessary data,
which is the needed metadata and file contents. Data like permissions, creation times or alternative data
streams are not written, as this functionality will be outsourced to plugins. But do not worry, as plugins
for basic data like permissions and creation/modification times will also be provided by this project.