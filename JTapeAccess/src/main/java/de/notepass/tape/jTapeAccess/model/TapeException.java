package de.notepass.tape.jTapeAccess.model;

import java.io.IOException;

public class TapeException extends IOException {
    private final ErrorCode errorCode;

    public static enum ErrorCode {
        /**
         * The given device file could not be found or accessed
         */
        DEVICE_NOT_FOUND,

        /**
         * No read operations are currently possible on the device but where attempted
         */
        DEVICE_NOT_READABLE,

        /**
         * No write operations are currently possible on the device but where attempted
         */
        DEVICE_NOT_WRITABLE,

        /**
         * There was an error, but no direct code exists for it. Might be an device, media, access, API, OS or whatever error
         */
        DEVICE_MISC_ERROR,

        /**
         * Tried to access tape information or read/write operations while there is no tape in the device
         */
        NO_TAPE_IN_DRIVE,

        /**
         * Tried to do an operation at an invalid tape position
         */
        INVALID_TAPE_POSITION,

        /**
         * Shitters clogged
         */
        NO_SPACE_LEFT,

        /**
         * There are no native mappings to access tape drives for this OS<br/>
         * Maybe contribute to the project by implementing it? :)
         */
        NO_NATIVE_IMPL_AVAILABLE
    }

    public TapeException(ErrorCode errorCode, String message, Throwable cause) {
        super(message, cause);
        this.errorCode = errorCode;
    }

    public TapeException(ErrorCode errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }
}
