package de.notepass.tape.jTapeAccess.model;

public enum DriveCapabilities {
    /**
     * Drives supports hardware compression
     */
    COMPRESSION,

    /**
     * Drives supports hardware encryption
     */
    ENCRYPTION,

    /**
     * Drives supports data padding (Tape drive will always move at the same speed while writing and just fill data if
     * there is none buffered on the drive)
     */
    DATA_PADDING,

    /**
     * Drive supports checking contents while writing/reading
     */
    ECC
}
