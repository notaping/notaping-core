# ETFS data structure
This file contains information about the structure of ETFS on the physical tape.

## Theoretical limits
Following is a list of limits opposed by the underlying data structure. Most of the limits where chosen in a
way that should keep these limits theoretical and not reacheable in practise when using the software properly
- The maximum length of the tape label is 32767 bytes. As the label is UTF-8 encoded this is about 32767 characters,
  although legacy ASCII characters and multi byte characters (e.g. some emojis) can mean that more or less characters
  are actually available 
- A maximum of 32767 plugins can be enabled when writing a tape
- A maximum of 32767 tapes can be used for a multitape volume (Even when using uncompressed LTO-1 this is still ~3TB)
- The maximum length of a plugin ID is 32767 bytes
- The maximum size of a file on the tape is 9223372036854775807 bytes (That's 8 Exabyte!)
- The maximum amount of files in a multitape volume (or one tape) is 9223372036854775807
- The maximum length for a file name (This includes the file path) is 32767 bytes
- The maximum age of a rabbit is 16 years (This is less a restriction of this program and more an unfortunate circumstance of existence)
- The maximum size of the configuration part which each plugin could save to the tape is 2147483000 bytes
- The maximum size of custom data that can be written after the header, after each TOC entry and after each content entry is 2147483000 bytes

## How to read this document
The subchapters for the tape sections will describe which data is written to the tape. To do this,
the contents will be written in the order, as they are on tape, followed by the size in byte and
the data type. If the data type is marked as struct, it will be explained in its own subchapter.  
For some elements the size will be given as <DYN>, meaning that the size is not limited or predefined.
Additionally, there may be entries in the format n:m. This means, that the entry has a size of at least
n, but maximum m bytes.  
Data types used in this document are:
- float: Indicative of a floating-point number. This may be a float, double or something else
- int: Indicative of a number with no decimal part. May be int, long or something else
- string: Well, as always: Text. Is to be encoded in UTF-8
- byte: Some other binary data (e.g. file contents)
- struct<Struct name>: Composite of the data type given in this list
- list<Struct type>: A list of elements (aka multiple entries of the same element after each other)

An entry is described as following:  
``<Position>. <Name>: <min:max bytes> <datatype>: <Description>``  
or  
``<Position>. <Name>: <number bytes> <datatype>: <Description>``

In case the entry type is "list" the (min/max) number of bytes is to be treated as the number of entries.
Additionally, a list can only contain a reference to a struct type, as everything else would make describing
the content with a simple syntax pretty hard. This can, in some places, lead to single entry structs.

All numbers should be seen as signed and serialized in [Two's complement](https://en.wikipedia.org/wiki/Two's_complement) fashion.

## Base overview
On the root level, ETFS is split into the following 4 parts:
1. Header: Contains tape metadata (e.g. the plugin list, name of the tape, ID and magic)
2. Plugin config: Contains the configuration of used plugins (or what the plugins want to disclose as configuration) to
   protect from read problems because of different configurations
2. Table of contents: The TOC contains a list of files, which is stored on the tape
3. File contents: The raw contents of the files  

The content of these parts is explained in more detail in the according chapters.

## Header section
The header section contains the following data:
1. magic: 8 string: "Magic string" to indicate ETFS tape. Is always "//ETFS//"
2. version: 4 int: Version of the ETFS core used to create the tape
3. min_version: 4 int: Minimum ETFS core version which is necessary to read this tape
4. allow_multitape: 1 int: Boolean value, indicating if this tape can be seen as a possible multitape volume (See WIKI)
5. multitape_index: 2 int: The index of this tape in a multitape volume. Starts at 0 for the first tape.
6. volume_id_size: 1 int: Size of the tape volume_id in bytes
6. volume_id: 0:126 string: ID of the volume, for use with multitape volumes
6. label_size: 2 int: Size of the tape label in bytes
7. label: 0:32767 string: A user-defined, human readable, label for the tape (Or some other stuff, as long as it can be stringed. I'm a doc,not a cop)

## Plugin config section
The plugin config section contains the parts of the plugin-config seen as "essentail" by a plugin to make sure, it
can be correctly executed for reading its own data. It is set up as follows:
1. plugin_info_length: 2 int: Number of entries in the plugin_info list
2. plugin_info: 0:32767 list<PluginInfo>: The list of plugins used when creating this tape and configuration metadata for them
3. entries_length: 2 int: Number of entries in the entries list
4. entries: 0:32767 list<PluginConfigEntry>: List of configuration entries
5. plugin_data_length: 2 int: Number of entries in the entries list
6. plugin_data: 0:32767 list<PluginCustomData>: Space for plugins to write custom data to (AFTER_HEADER)

### Struct: PluginInfo
1. size: 4 int: Size of the entry, excluding this field
2. version: 4 int: Version of the plugin used to create this entry
3. id_size: 2 int: Size of the identifier string of the plugin (See the "creating a plugin" doc for more information on this)
4. id: 1:32767 string: ID of the plugin which is described in this section (See the "creating a plugin" doc for more information on this)
5. internal_id: 2 int: Remapped ID to a 16-bit integer. This is done to save space, as this ID will be referenced instead of the
   (probably) longer "real" ID. This also means, that a max of 32767 plugins can be active per tape. (See the "creating a plugin" doc for more information on this)

### Struct: PluginConfigEntry
1. size: 4 int: Size of the entry, excluding this field
2. internal_id: 2 int: Reference to the internal ID from the header section
3. content: 2147483000 byte: Configuration file content. Advised to be string, but can be any data format
4. plugin_data_metadata: 4 int: Flags to manage problems that happened on write time (See plugin_data_metadata for more information)

### Struct: PluginCustomData
1. size: 4 int: Size of the entry, excluding this field
2. internal_id: 2 int: Reference to the internal ID from the header section
3. content: 2147483000 byte: Custom data content. Advised to be string, but can be any data format
4. plugin_data_metadata: 4 int: Flags to manage problems that happened on write time (See plugin_data_metadata for more information)

## Table of contents section
The TOC contains metadata about files (e.g. file paths and last changed dates). It is build up as follows:
1. file_entries_length: 8 int: Number of entries in the file_entries list
2. file_entries: 0:9223372036854775807 list<FileEntry>: List of file metadata present

### Struct: FileEntry
1. filename_size: 2 int: Size of the filename field
2. filename: 0:32767 string: Path and name of the file saved (normalized, see WIKI)
3. file_meta: 8 int: Additional file meta data (See Metadata section for more info)
4. plugin_data_length: 2 int: Number of entries in the plugin_data list
5. plugin_data: 0:32767 list<PluginCustomData>: Space for plugins to write custom data to (AFTER_TOC_ENTRY)

## File contents section
The file content section contains the main file contents and additional plugin metadata for them. It is build up as following:
1. files: 0:9223372036854775807 List<FileContent>: List of files

The size of the files list can be acquired in the table of contents section

### Struct: FileContent
1. file_size: 8 int: Size of the file in bytes
2. file_content: 0:9223372036854775807 byte: Content of the file. Size is dictated by the file_size filed in the TOC section
3. runtime_meta: 4 int: Additional metadata resulting of possible runtime problems when reading/writing the file (See Metadata section for more info)
4. plugin_data_length: 2 int: Number of entries in the plugin_data list
5. plugin_data: 0:32767 list<PluginCustomData>: Space for plugins to write custom data to (AFTER_FILE_CONTENT)

Afterwards, an EOF mark is written to the tape. This isn't linked to any field, as it is a feature of the physical media.


# Metadata structure
This section describes the setup of the file_meta and runtime_meta fields.

## Field: file_meta
Size: 8bytes (64 bit).  
Each bit is treated as a flag. The bits are used as follows:
0. is_multitape_part: If set to 1, the content is not the whole file content, but only a part of it. This happens if the
   volume is a multitape volume where file content was already written to a previous tape.
1. to 63: Unused

## Field: runtime_meta
Size: 4bytes (32 bit).  
Each bit is treated as a flag. The bits are used as follows:
0. read_error: If set to 1, an error occured while reading the file. If this flag is set, the file content on the tape 
   should be disregarded!
1. write_error: If set to 1, an error occured while writing the file to tape the file. If this flag is set, the file 
   content on the tape should be disregarded!
2. to 31: Unused

## Field: plugin_data_metadata
Size: 4bytes (32 bit). Contains Information about problems that occured while generating plugin custom data/config entries  
Each bit is treated as a flag. The bits are used as follows:
0. plugin_error: If set to 1, the plugin threw an unhandled exception while generating the data. No data was written
   and this entry should be ignored when reading
1. to 31: Unused