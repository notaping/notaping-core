package de.notepass.notaping.core.fs.etfs.data;

import de.notepass.notaping.core.fs.etfs.data.listener.*;
import de.notepass.notaping.core.fs.etfs.data.section.FileSection;
import de.notepass.notaping.core.fs.etfs.data.section.HeaderSection;
import de.notepass.notaping.core.fs.etfs.data.section.PluginConfigSection;
import de.notepass.notaping.core.fs.etfs.data.section.TableOfContentSection;
import de.notepass.notaping.core.fs.etfs.plugin.EtfsPlugin;
import de.notepass.notaping.core.fs.etfs.plugin.PluginRegistry;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;

public class EtfsInstance {
    // Plugins which should be used when writing or were detected when reading
    private PluginRegistry pluginRegistry;

    // Main FS sections
    private HeaderSection headerSection;
    private PluginConfigSection pluginConfigSection;
    private TableOfContentSection tableOfContentSection;
    private FileSection fileSection;

    // Current tape in multitape volume
    private short tapeIndex;

    protected EtfsInstance() {

    }

    /**
     * Returns the plugin registry for this ETFS instance
     * @return
     */
    public PluginRegistry getPluginRegistry() {
        return pluginRegistry;
    }

    /**
     * Sets the plugin manager for this ETFS instance
     * @param pluginRegistry
     */
    public void setPluginRegistry(PluginRegistry pluginRegistry) {
        this.pluginRegistry = pluginRegistry;
    }

    /**
     * Returns the main header section of the filesystem (if present)
     * @return
     */
    public HeaderSection getHeaderSection() {
        return headerSection;
    }

    /**
     * Sets the main header section of the filesystem
     */
    public void setHeaderSection(HeaderSection headerSection) {
        this.headerSection = headerSection;
    }

    /**
     * Returns the main plugin config section of the filesystem (if present)
     * @return
     */
    public PluginConfigSection getPluginConfigSection() {
        return pluginConfigSection;
    }

    /**
     * Sets the main plugin config section of the filesystem
     */
    public void setPluginConfigSection(PluginConfigSection pluginConfigSection) {
        this.pluginConfigSection = pluginConfigSection;
    }

    /**
     * Returns the main TOC section of the filesystem (if present)
     * @return
     */
    public TableOfContentSection getTableOfContentSection() {
        return tableOfContentSection;
    }

    /**
     * Sets the main TOC section of the filesystem
     */
    public void setTableOfContentSection(TableOfContentSection tableOfContentSection) {
        this.tableOfContentSection = tableOfContentSection;
    }

    /**
     * Returns the main files section of the filesystem (if present)
     * @return
     */
    public FileSection getFileSection() {
        return fileSection;
    }

    /**
     * Sets the main files section of the filesystem
     */
    public void setFileSection(FileSection fileSection) {
        this.fileSection = fileSection;
    }

    /**
     * Current index in multitape volume, starts at 0
     * @return
     */
    public short getTapeIndex() {
        return tapeIndex;
    }

    /**
     * Current index in multitape volume, starts at 0
     * @param tapeIndex
     */
    public void setTapeIndex(short tapeIndex) {
        this.tapeIndex = tapeIndex;
    }

    public void serialize(OutputStream os, EtfsWriteProgressCallback callback) throws IOException {
        headerSection.serialize(os, this, callback);
        pluginConfigSection.serialize(os, this, callback);
        tableOfContentSection.serialize(os, this, callback);
        fileSection.serialize(os, this, callback);
    }

    public void deserialize(InputStream is, EtfsReadProgressCallback callback) throws IOException {
        headerSection.deserialize(is, -1, this, callback);
        int version = headerSection.getVersion();
        pluginConfigSection.deserialize(is, version, this, callback);
        tableOfContentSection.deserialize(is, version, this, callback);
        fileSection.deserialize(is, version, this, callback);
    }
}
