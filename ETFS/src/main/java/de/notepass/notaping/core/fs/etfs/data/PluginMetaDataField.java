package de.notepass.notaping.core.fs.etfs.data;

public class PluginMetaDataField {
    public enum PLUGIN_METADATA_FLAG {
        /**
         * Error state indicating that the plugin generated an unhandeled exception while
         */
        PLUGIN_ERROR(0x000001);

        private int mask;

        PLUGIN_METADATA_FLAG(int mask) {
            this.mask = mask;
        }

        public int getSetMask() {
            return mask;
        }

        public int getUnsetMask() {
            return mask ^ 0xFFFFFF;
        }
    }

    private int pluginDataMetadata;

    public void setFlag(PLUGIN_METADATA_FLAG flag) {
        pluginDataMetadata |= flag.getSetMask();
    }

    public boolean getFlag(PLUGIN_METADATA_FLAG flag) {
        return (pluginDataMetadata & flag.getSetMask()) != 0;
    }

    public void unsetFlag(PLUGIN_METADATA_FLAG flag) {
        pluginDataMetadata &= flag.getUnsetMask();
    }

    public int getPluginDataMetadata() {
        return pluginDataMetadata;
    }

    public void setPluginDataMetadata(int pluginDataMetadata) {
        this.pluginDataMetadata = pluginDataMetadata;
    }
}
