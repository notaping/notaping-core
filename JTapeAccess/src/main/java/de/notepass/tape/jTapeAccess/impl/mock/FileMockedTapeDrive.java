package de.notepass.tape.jTapeAccess.impl.mock;

import de.notepass.tape.jTapeAccess.model.DriveCapabilities;
import de.notepass.tape.jTapeAccess.model.SimpleTapeDrive;
import de.notepass.tape.jTapeAccess.model.TapeException;
import de.notepass.tape.jTapeAccess.model.TapeInfo;

import java.io.*;

/**
 * A tape drive that isn't a tape drive, but a file. Used for testing purposes or to play around with the library
 */
public class FileMockedTapeDrive implements SimpleTapeDrive {
    private long blockSize = 64 * 1024;
    private InputStream is;
    private OutputStream os;
    private File file;

    public FileMockedTapeDrive(File file, boolean readAccess, boolean writeAccess) throws FileNotFoundException, TapeException {
        this.file = file;
        if (readAccess) {
            initReadAccess();
        }

        if (writeAccess) {
            initWriteAccess();
        }
    }

    protected synchronized void initReadAccess() throws TapeException {
        if (is == null) {
            if (os != null) {
                // Already in write mode - deny read access
                throw new TapeException(TapeException.ErrorCode.DEVICE_MISC_ERROR, "Read access requested while write access is already established");
            }

            try {
                is = new FileInputStream(file);
            } catch (FileNotFoundException e) {
                throw new TapeException(TapeException.ErrorCode.DEVICE_MISC_ERROR, "Could not create InputStream", e);
            }
        }
    }

    protected synchronized void initWriteAccess() throws TapeException {
        if (os == null) {
            if (is != null) {
                // Already in write mode - deny read access
                throw new TapeException(TapeException.ErrorCode.DEVICE_MISC_ERROR, "Write access requested while read access is already established");
            }

            try {
                os = new FileOutputStream(file);
            } catch (FileNotFoundException e) {
                throw new TapeException(TapeException.ErrorCode.DEVICE_MISC_ERROR, "Could not create OutputStream", e);
            }
        }
    }

    @Override
    public void loadTape() throws TapeException {

    }

    @Override
    public void ejectTape() throws TapeException {

    }

    @Override
    public boolean isReady() throws TapeException {
        return true;
    }

    @Override
    public TapeException getErrors() {
        return null;
    }

    @Override
    public boolean isReadable() throws TapeException {
        return true;
    }

    @Override
    public boolean isWritable() throws TapeException {
        return true;
    }

    @Override
    public long getTapePosition() throws TapeException {
        return 0;
    }

    @Override
    public void setTapePosition(long pos) throws TapeException {

    }

    @Override
    public void forwardToNextFilemark() throws TapeException {

    }

    @Override
    public void reverseToPreviousFilemark() throws TapeException {

    }

    @Override
    public DriveCapabilities[] getDriveCapabilities() throws TapeException {
        return new DriveCapabilities[]{DriveCapabilities.COMPRESSION, DriveCapabilities.DATA_PADDING, DriveCapabilities.ECC, DriveCapabilities.ENCRYPTION};
    }

    @Override
    public void enableDriveCapability(DriveCapabilities... driveCapabilities) throws TapeException {

    }

    @Override
    public void disableDriveCapability(DriveCapabilities... driveCapabilities) throws TapeException {

    }

    @Override
    public void setMediumBlockSize(long blockSizeBytes) throws TapeException {
        blockSize = blockSizeBytes;
    }

    @Override
    public int readBytes(byte[] buffer, int offset, int numBytes) throws TapeException {
        try {
            return is.read(buffer, offset, numBytes);
        } catch (IOException e) {
            throw new TapeException(TapeException.ErrorCode.DEVICE_MISC_ERROR, "IOException in input stream", e);
        }
    }

    @Override
    public int writeBytes(byte[] buffer, int offset, int numBytes) throws TapeException {
        try {
            os.write(buffer, offset, numBytes);
            return buffer.length;
        } catch (IOException e) {
            throw new TapeException(TapeException.ErrorCode.DEVICE_MISC_ERROR, "IOException in output stream", e);
        }
    }

    @Override
    public int readBytes(byte[] buffer) throws TapeException {
        try {
            return is.read(buffer);
        } catch (IOException e) {
            throw new TapeException(TapeException.ErrorCode.DEVICE_MISC_ERROR, "IOException in input stream", e);
        }
    }

    @Override
    public int writeBytes(byte[] buffer) throws TapeException {
        try {
            os.write(buffer);
            return buffer.length;
        } catch (IOException e) {
            throw new TapeException(TapeException.ErrorCode.DEVICE_MISC_ERROR, "IOException in output stream", e);
        }
    }

    @Override
    public void writeEofMark(int count) throws TapeException {

    }

    @Override
    public OutputStream getOutputStream() throws TapeException {
        return os;
    }

    @Override
    public InputStream getInputStream() throws TapeException {
        return is;
    }

    @Override
    public void flushCaches() throws TapeException {

    }

    @Override
    public boolean isMediaPresent() throws TapeException {
        return true;
    }

    @Override
    public TapeInfo getTapeInfo() throws TapeException {
        return new TapeInfo(200L * 1024 * 1024 * 1024, 200L * 1024 * 1024 * 1024, 400L * 1024 * 1024 * 1024, false, blockSize);
    }

    @Override
    public void partitionTape() throws TapeException {

    }
}
