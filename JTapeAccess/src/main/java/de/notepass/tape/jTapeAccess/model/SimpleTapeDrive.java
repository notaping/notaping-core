package de.notepass.tape.jTapeAccess.model;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public interface SimpleTapeDrive {
    /**
     * Loads the physical tape media into the tape drive (If present in a way this can be accomplished)
     * @throws TapeException
     */
    void loadTape() throws TapeException;

    /**
     * Ejects the tape from the tape drive
     * @throws TapeException
     */
    void ejectTape() throws TapeException;

    /**
     * Returns true if the tape drive is ready to be written to / read from.<br>
     * More information can be gained by calling {@link #getErrors()}
     * @return
     * @throws TapeException
     */
    boolean isReady() throws TapeException;

    /**
     * Returns the errors currently encountered by the tape drive
     * @return
     */
    TapeException getErrors();

    /**
     * Returns true if the media can be read from
     * @return
     * @throws TapeException
     */
    boolean isReadable() throws TapeException;

    /**
     * Returns true if the media can be written to
     * @return
     * @throws TapeException
     */
    boolean isWritable() throws TapeException;

    /**
     * Returns the current block the media is positioned at
     * @return
     * @throws TapeException
     */
    long getTapePosition() throws TapeException;

    /**
     * Sets the block address the drive should move the media to
     * @param pos
     * @throws TapeException
     */
    void setTapePosition(long pos) throws TapeException;

    /**
     * Fast forwards the media to the next filemark
     * @throws TapeException
     */
    void forwardToNextFilemark() throws TapeException;

    /**
     * Reverses the media to the previous filemark
     * @throws TapeException
     */
    void reverseToPreviousFilemark() throws TapeException;

    /**
     * Returns the functionallities the drive implements. These can be turned on or off via {@link #enableDriveCapability(DriveCapabilities)} and {@link #disableDriveCapability(DriveCapabilities)}
     * @return
     * @throws TapeException
     */
    DriveCapabilities[] getDriveCapabilities() throws TapeException;

    /**
     * Enabled a drive functionality
     * @param driveCapabilities
     * @throws TapeException
     */
    void enableDriveCapability(DriveCapabilities... driveCapabilities) throws TapeException;

    /**
     * Disables a drive functionality
     * @param driveCapabilities
     * @throws TapeException
     */
    void disableDriveCapability(DriveCapabilities... driveCapabilities) throws TapeException;

    /**
     * Sets the block size used for writing to and reading from the medium
     * A default value of 64KiB will be set on initialization.<br/>
     * After this property is set all read and write operations must be done with a multiple of the given block size. So make sure to use buffered streams!
     * (BufferedOutputStream and BufferedTapeInputStream)<br/>
     * <b>Please note:</b> It is currently discouraged to use dynamic block sizing, as windows is just useless when enabling it!
     * @param blockSizeBytes
     */
    void setMediumBlockSize(long blockSizeBytes) throws TapeException;

    /**
     * Tires to read the given amount if bytes into the given buffer. Returns the actually read amount of bytes.
     * @param buffer Buffer to read data into
     * @param offset Offset inside the buffer into which to read the first byte
     * @param numBytes Number of bytes to read into the buffer
     * @return Actual number of bytes read
     * @throws TapeException
     */
    int readBytes(byte[] buffer, int offset, int numBytes) throws TapeException;

    /**
     * Tries to write the bytes given in the buffer-array onto the media. Returns the actually written amount of bytes
     * @param buffer Buffer to write onto the media
     * @param offset index of first byte to read from buffer and write to media
     * @param numBytes Number of bytes to write to the media
     * @return Actual number of bytes written
     * @throws TapeException
     */
    int writeBytes(byte[] buffer, int offset, int numBytes) throws TapeException;

    /**
     * Tires to read the given amount if bytes into the given buffer. Returns the actually read amount of bytes.
     * @param buffer Buffer to read data into
     * @return Actual number of bytes read
     * @throws TapeException
     */
    int readBytes(byte[] buffer) throws TapeException;


    /**
     * Tries to write the bytes given in the buffer-array onto the media. Returns the actually written amount of bytes
     * @param buffer Buffer to write onto the media
     * @return Actual number of bytes written
     * @throws TapeException
     */
    int writeBytes(byte[] buffer) throws TapeException;

    /**
     * Writes filemarks at the given position
     * @param count Amount of filemarks to write
     * @throws TapeException
     */
    void writeEofMark(int count) throws TapeException;

    /**
     * Returns an OutputStream representation of the tape
     * @return
     * @throws TapeException
     */
    OutputStream getOutputStream() throws TapeException;

    /**
     * Returns an InputStream representation of the tape
     * @return
     * @throws TapeException
     */
    InputStream getInputStream() throws TapeException;

    /**
     * Flush caches to media
     * @throws TapeException
     */
    void flushCaches() throws TapeException;

    /**
     * Returns true if a media is present int the drive
     * @return
     */
    boolean isMediaPresent() throws TapeException;

    /**
     * Returns information about the currently inserted tape
     * @return
     */
    TapeInfo getTapeInfo() throws TapeException;

    /**
     * Creates a single partition on the tape, spanning the whole size of the tape
     */
    void partitionTape() throws TapeException;
}
