package de.notepass.notaping.core.fs.etfs.plugin;

public enum MetadataPosition {
    AFTER_HEADER, AFTER_TOC_ENTRY, AFTER_FILE_CONTENT
}
