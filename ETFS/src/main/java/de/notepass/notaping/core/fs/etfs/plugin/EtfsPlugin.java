package de.notepass.notaping.core.fs.etfs.plugin;

import de.notepass.notaping.core.fs.etfs.data.section.HeaderSection;
import de.notepass.notaping.core.fs.etfs.data.struct.FileContent;
import de.notepass.notaping.core.fs.etfs.data.struct.FileEntry;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Path;

public abstract class EtfsPlugin {

    /**
     * Remapped ID, which is only valid for a given tape or multitape volume. This is the alias your plugin should listen
     * to
     */
    protected short internalId;

    /**
     * Returns the version of the plugin
     * @return
     */
    public abstract int getVersion();

    /**
     * Returns the unique ID of the plugin. Can be e.g. the FQCN
     * @return
     */
    public abstract String getID();




    /**
     * Returns if this plugin must serialize critical configuration parameters to the tape. These are configuration parts
     * which cannot differ between different versions of the plugin, as else reading the custom data in other parts
     * of the tape would fail
     * @return true if data should be written, else false
     */
    public abstract boolean wantsToWriteRequiredConfigForReadOperation();

    /**
     * This method should write configuration parameters, without which the plugin data could not be read correctly
     * on another system with the same plugin but a different configuration (e.g. setting the charset the plugin uses)
     * to the tape.
     */
    public abstract void writeRequiredConfigForReadOperation(OutputStream os) throws IOException;

    /**
     * This method should read configuration parameters, without which the plugin data could not be read correctly
     * on another system with the same plugin but a different configuration (e.g. setting the charset the plugin uses)
     * to the tape.
     * @param version The version of the plugin used when writing the information
     */
    public abstract void readRequiredConfigForReadOperation(InputStream is, int version, int dataSize) throws IOException;


    /* ============================= AFTER_HEADER ============================= */

    /**
     * Returns weather the plugin wants to write data after the header section. The data will be written at the end
     * of the PluginConfig section physically on the tape
     * @param metadataPosition Position of the metadata field on the tape. In this case {@link MetadataPosition#AFTER_HEADER}
     * @param context HeaderSection, in case the plugin needs to read data from it
     * @return true if the plugin wants to serialize data to the plugin custom data section given
     */
    public abstract boolean wantsToWritePluginData(MetadataPosition metadataPosition, HeaderSection context);

    /**
     * Writes the plugin custom data to the given section
     * @param metadataPosition Position of the metadata field on the tape. In this case {@link MetadataPosition#AFTER_HEADER}
     * @param context HeaderSection, in case the plugin needs to read data from it
     * @param os OutputStream to write to
     */
    public abstract void writePluginData(MetadataPosition metadataPosition, HeaderSection context, OutputStream os) throws IOException;

    /**
     * Reads the plugin custom data from the given section
     * @param metadataPosition Position of the metadata field on the tape. In this case {@link MetadataPosition#AFTER_HEADER}
     * @param context HeaderSection, in case the plugin needs to read data from it
     * @param is InputStream to read from
     * @param version Version of the plugin used to serialize this data
     * @param dataSize The amount of bytes which must be read to access all of the saved data
     */
    public abstract void readPluginData(MetadataPosition metadataPosition, HeaderSection context, InputStream is, int version, int dataSize) throws IOException;

    /* ============================= AFTER_TOC_ENTRY ============================= */

    /**
     * Returns weather the plugin wants to write data after a given file emtry in the TOC section. The data will be
     * written at the end of the given FileEntry struct physically on the tape
     * @param metadataPosition Position of the metadata field on the tape. In this case {@link MetadataPosition#AFTER_TOC_ENTRY}
     * @param context FileEntry struct, in case the plugin needs to read data from it
     * @return true if the plugin wants to serialize data to the plugin custom data section given
     */
    public abstract boolean wantsToWritePluginData(MetadataPosition metadataPosition, FileEntry context);

    /**
     * Writes the plugin custom data to the given section
     * @param metadataPosition Position of the metadata field on the tape. In this case {@link MetadataPosition#AFTER_TOC_ENTRY}
     * @param context HeaderSection, in case the plugin needs to read data from it
     * @param os OutputStream to write to
     */
    public abstract void writePluginData(MetadataPosition metadataPosition, FileEntry context, OutputStream os) throws IOException;

    /**
     * Writes the plugin custom data to the given section
     * @param metadataPosition Position of the metadata field on the tape. In this case {@link MetadataPosition#AFTER_TOC_ENTRY}
     * @param context FileEntry struct, in case the plugin needs to read data from it
     * @param is InputStream to read from
     * @param version Version of the plugin used to serialize this data
     * @param dataSize The amount of bytes which must be read to access all of the saved data
     */
    public abstract void readPluginData(MetadataPosition metadataPosition, FileEntry context, InputStream is, int version, int dataSize) throws IOException;

    /* ============================= AFTER_FILE_CONTENT ============================= */

    /**
     * Returns weather the plugin wants to write data after a given file content. The data will be written at the end
     * of the FileContent struct physically on the tape
     * @param metadataPosition Position of the metadata field on the tape. In this case {@link MetadataPosition#AFTER_FILE_CONTENT}
     * @param context HeaderSection, in case the plugin needs to read data from it
     * @param file Reference to the file written to the tape. The content has already been written. The plugin can open
     *             streams on the file, but MUST close these before exiting. (Please use try-catch with resources, I
     *             cannot clean up behind you here)
     * @return true if the plugin wants to serialize data to the plugin custom data section given
     */
    public abstract boolean wantsToWritePluginData(MetadataPosition metadataPosition, FileContent context, Path file);

    /**
     * Writes the plugin custom data to the given section
     * @param metadataPosition Position of the metadata field on the tape. In this case {@link MetadataPosition#AFTER_FILE_CONTENT}
     * @param context FileEntry struct, in case the plugin needs to read data from it
     * @param file Reference to the file written to the tape. The content has already been written. The plugin can open
     *             streams on the file, but MUST close these before exiting. (Please use try-catch with resources, I
     *             cannot clean up behind you here)
     * @param os OutputStream to write to
     */
    public abstract void writePluginData(MetadataPosition metadataPosition, FileContent context, Path file, OutputStream os) throws IOException;

    /**
     * Reads the plugin custom data from the given section
     * @param metadataPosition Position of the metadata field on the tape. In this case {@link MetadataPosition#AFTER_FILE_CONTENT}
     * @param context FileEntry struct, in case the plugin needs to read data from it
     * @param file Reference to the file written to the tape. The content has already been written. The plugin can open
     *             streams on the file, but MUST close these before exiting. (Please use try-catch with resources, I
     *             cannot clean up behind you here)
     * @param is InputStream to read from
     * @param version Version of the plugin used to serialize this data
     * @param dataSize The amount of bytes which must be read to access all of the saved data
     */
    public abstract void readPluginData(MetadataPosition metadataPosition, FileContent context, Path file, InputStream is, int version, int dataSize) throws IOException;



    /**
     * Remapped ID, which is only valid for a given tape or multitape volume
     * @return
     */
    public short getInternalId() {
        return internalId;
    }

    /**
     * Remapped ID, which is only valid for a given tape or multitape volume
     * @param internalId
     */
    public void setInternalId(short internalId) {
        this.internalId = internalId;
    }
}
